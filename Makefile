SHELL=/bin/bash

NODEJSAPP_SERVICE_NAME=nodejsapp
NODEJSAPP_IMAGE=nodejsapp
NETWORK=interna

NODEJSAPP_PORTS_EXPOSED="3000:3000"

all:	build network up

.PHONY:	build network up rmnetwork clean cleanall down downrmi stop rmi rm

build:	
	docker build -t ${NODEJSAPP_IMAGE} . 

network:	
	- docker network create -d bridge ${NETWORK}

up:	network
	docker run -itd --rm --network interna --name ${NODEJSAPP_SERVICE_NAME} -p 3000:3000 ${NODEJSAPP_SERVICE_NAME}

cleanall:	downrmi

clean:	down

downrmi: down  rmi

down:	stop rm rmnetwork

stop:	
	- docker stop ${NODEJSAPP_SERVICE_NAME}

rm:	
	- docker rm ${NODEJSAPP_SERVICE_NAME}

rmi:	
	- docker rmi ${NODEJSAPPIMAGE}

rmnetwork:	
	- docker network rm ${NETWORK}