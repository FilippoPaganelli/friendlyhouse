const mongoose = require('mongoose')
const axios = require('axios')
DeviceActivityLog = mongoose.model('deviceactivitylogmodel')
const HttpStatus = require('http-status-codes');


exports.global = async function(req, res) {
  const from = req.params.from
  const to = new Date(req.params.to)

  console.log(from)
  console.log(to)
  const current = (new Date(to.getFullYear(), to.getMonth() + 2, 1)).toISOString().substring(0,7)
  console.log(current)

  await DeviceActivityLog
    .aggregate([
      {$match: {
        timestamp: {
          $gte: new Date(from),
          $lte: new Date(current),
      }}},
      {$lookup: {
        from: 'DeviceType',
        localField: 'device_model',
        foreignField: 'model',
        as: 'device_type_detail'
      }},
      {$match: {
        "device_type_detail.category": {
          $nin: ['temp']
        }
      }},
      {$project: {
        device: 1,
        device_model: 1,
        new_state: 1,
        timestamp: 1,
        device_type_detail: {
          $arrayElemAt: ['$device_type_detail', 0]
        },
        _id: 0
      }}
    ]).exec((err, result) => {
      let mapped = {}
      let toReturn = {}



      result.forEach((item, i) => {

        let date = item.timestamp.toISOString().substring(0, 10)
        let device = item.device

        if (! mapped.hasOwnProperty(date)) {
          mapped[date] = {}
          toReturn[date] = {}
          toReturn[date]['device_consumption'] = {}
          toReturn[date]['total_time'] = 0
          toReturn[date]['total_wattage'] = 0
        }

        if(! mapped[date].hasOwnProperty(device)){
          mapped[date][device] = []
          toReturn[date]['device_consumption'][device] = {
            'total_time' : 0,
            'total_wattage': 0
          }
        }

        mapped[date][device].push(item)
      })

      let comulativeTime = 0
      let comulativeWattage = 0
      for (const [date, dateValue] of Object.entries(mapped)) {
        let dayTime = 0
        let dayWattage = 0
        //let dayConsumption = 0
        console.log(date)
        for (const [device, deviceValue] of Object.entries(dateValue)) {
          console.log(device)
          let currentState = null
          //ottenere valore in millisecondi del giorno
          let lastTime = new Date(date).getTime()
          let deviceTime = 0
          let deviceConsumption = 0
          console.log(deviceValue)
          let deviceWattage = deviceValue[0].device_type_detail.watt
          deviceValue.forEach((item) => {
            if (item.new_state == "true") {
              currentState = true
              lastTime = item.timestamp.getTime()
              //console.log(date, device, item.timestamp, item.timestamp.getTime() / 3.6e6, "ho acceso")
            }
            else if (item.new_state == "false") {
              currentState = false
              deviceTime += (item.timestamp.getTime() - lastTime)
              //console.log(date, device, item.timestamp, item.timestamp.getTime() / 3.6e6, "ho spento", item.timestamp.getTime() - lastTime)
              lastTime = item.timestamp.getTime()
            }
          });
          dayTime += deviceTime
          dayWattage += (deviceTime / 3.6e6 * deviceWattage)
          toReturn[date]['device_consumption'][device]['total_time'] = deviceTime / 3.6e6
          toReturn[date]['device_consumption'][device]['total_wattage'] = deviceTime / 3.6e6 * deviceWattage
          if (currentState) {
            //considerare uno spegnimento a mezza notte nel computo del considerato
            //per qualche ragione per ottenerele 23 occorre inserire le 25
            let midnight =  new Date(date).setHours(25, 59, 59);
            //console.log("Hai lasciato acceso!", date, new Date(midnight),((midnight - lastTime) / 3.6e6))
            //aggiungere al device tempo e wattage
            toReturn[date]['device_consumption'][device]['total_time'] += ((midnight - lastTime) / 3.6e6)
            toReturn[date]['device_consumption'][device]['total_wattage'] += ((midnight - lastTime) / 3.6e6 * deviceWattage)
            dayTime += (midnight - lastTime)
            dayWattage += ((midnight - lastTime) / 3.6e6 * deviceWattage)

          }
        }
        toReturn[date]['total_time'] = dayTime / 3.6e6
        toReturn[date]['total_wattage'] = dayWattage
        comulativeTime += toReturn[date]['total_time']
        comulativeWattage += toReturn[date]['total_wattage']
      }
      toReturn['total_time'] = comulativeTime
      toReturn['total_wattage'] = comulativeWattage

      res.status(HttpStatus.OK).send(toReturn)
    })

}
