const mongoose = require('mongoose')
const DeviceActivityLog = require("../models/deviceActivityLogModel")(mongoose)
const HttpStatus = require('http-status-codes');


exports.getLastDeviceActivityLog = async function (req, res) {
	DeviceActivityLog.find({device: req.params.deviceID})
		.sort('-timestamp')
		.limit(1)
		.then(log => {res.json(log[0])})
		.catch(err => res.status(HttpStatus.NOT_FOUND).send(err))
}

exports.getAllDeviceActivityLog = async function (req, res) {
	DeviceActivityLog.find({device: req.params.deviceID})
		.then(log => {res.json(log)})
		.catch(err => res.status(HttpStatus.NOT_FOUND).send(err))
}

exports.addDeviceActivityLog = async function (req, res) {
  const new_state = req.body.new_state.toString()
  const moved_to = req.body.moved_to
  const deviceID = req.params.deviceID


  const info = await DeviceActivityLog.find({device: req.params.deviceID})
      .sort('-timestamp')
      .limit(1)
  var lastInfo
  if (info.length == 0) {
    lastInfo = {
      new_state: undefined,
      moved_to: undefined
    }
  } else {
    lastInfo = info[0]
  }

  if (new_state != lastInfo.new_state || moved_to != lastInfo.moved_to){
    const deviceLog = new DeviceActivityLog({
  		user: req.body.user,
  		device: deviceID,
      device_model: req.body.deviceModel,
  		new_state: new_state,
  		moved_to: moved_to
  	})
  	try {
          const savedLog = await deviceLog.save()
          res.status(HttpStatus.CREATED).send({log: deviceLog._id})
      }catch(err){
          res.status(400).send(err)
      }
  } else { 
    console.log("no need to save")
    res.status(HttpStatus.OK)
  }


}

exports.getAll = async function(req, res) {
  const from = req.params.from
  const to = req.params.to
  DeviceActivityLog
    .find({
      timestamp: {
        $gte: (from),
        $lte: (to),
      }
    })
    .then(response => res.status(200).send(response))
}
