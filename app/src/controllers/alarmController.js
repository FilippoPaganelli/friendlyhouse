const mongoose = require('mongoose')
const Alarm = require("../models/alarmModel.js")(mongoose)
const bcrypt = require('bcryptjs')
const HttpStatus = require('http-status-codes')

exports.add = async function (req, res) {
    if(!req.body.pin || req.body.pin == "")
        res.status(HttpStatus.BAD_REQUEST).send("cannot set new pin")
    else{
        const salt = await bcrypt.genSalt(10)
        const hashPin = await bcrypt.hash(req.body.pin, salt)
        const pin = new Alarm({
            pin: hashPin,
            timestamp: Date.now()
        })
    
        try{
            const savedPin = await pin.save()
            res.status(HttpStatus.CREATED).json(savedPin)
        }catch(err) {
            console.log(err)
            res.status(HttpStatus.BAD_REQUEST).send("cannot set new pin")
        }
    }
}

exports.update = async function (req, res) {
    if(!req.body.pin || req.body.pin == "")
        res.status(HttpStatus.BAD_REQUEST).send("cannot set new pin")
    else {
        const salt = await bcrypt.genSalt(10)
        const hashPin = await bcrypt.hash(req.body.pin, salt)
    
        const result = await Alarm.updateOne({}, {$set: {pin: hashPin, timestamp: Date.now()}})
        if(!result)
            res.status(HttpStatus.BAD_REQUEST).send("cannot update pin")
        else
            res.status(HttpStatus.OK).send(result)
    }
}

exports.verify = async function (req, res) {
    if(!req.body.pin || req.body.pin == "")
        res.status(HttpStatus.BAD_REQUEST).send("cannot set new pin")
    else {
        const pin = await Alarm.find({})
        if(!pin)
            res.status(HttpStatus.NOT_FOUND).send("There is no pin")
        else {
            //validate pin
            const validPin = await bcrypt.compare(req.body.pin, pin[0].pin)
    
            if(!validPin)
                res.status(HttpStatus.BAD_REQUEST).send("Wrong pin")
            else
                res.status(HttpStatus.OK).send("OK")
        }
    }
}