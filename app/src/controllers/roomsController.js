const mongoose = require('mongoose')
const Room = require("../models/roomModel.js")(mongoose)
const axios = require('axios')
const HttpStatus = require('http-status-codes')

exports.getUserRooms = async function(req, res) {
    Room.find({name: {$ne: "dummy"}}, function(err, rooms){
        if(err)
            res.send(err)
        else {
            if(!rooms) 
                return res.status(HttpStatus.NOT_FOUND).send("There are no rooms")
            else
                res.json(rooms)
        }
    })
}

exports.getUserRoom = async function(req, res) {
    Room.find({name: req.params.name}, function(err, room){
        if(err)
            res.send(err)
        else {
            if(!room) 
                return res.status(HttpStatus.NOT_FOUND).send("There is no such room")
            else
                res.json(room)
        }
    })
}

exports.insertNewRoom = async function(req, res) {
    const room = new Room({
        name: req.body.name,
        img: req.body.img,
        link: "/",
        alarm_state: true,
        alarm_events: [],
        info: "null",
        description: "Visualizza e gestisci i dispositivi in "+req.body.name,
        devices: []
    })
    try {
        const savedRoom = await room.save()
        res.status(HttpStatus.CREATED).send({room: room._id})
    }catch(err){
        res.status(HttpStatus.BAD_REQUEST).send(err)
    }
}

exports.updateRoom = function (req, res) {
    Room.findById(req.params.id).then(room => {
        room.name = req.body.name
        room.img = req.body.img
        room.description = "Visualizza e gestisci i dispositivi presenti in "+req.body.name
        return room.save()
    }).then(room => {res.json(room)})
    .catch(err => res.status(HttpStatus.NOT_FOUND).send(err))
}

exports.deleteRoom = function (req, res) {
    Room.deleteOne({_id: req.params.id}, function(err, result) {
		if (err)
			res.send(err)
		else {
			if(result.deletedCount == 0)
				res.status(HttpStatus.NOT_FOUND).send({
					description: 'Room not found'
				})
			else
				res.json({ message: 'Room successfully deleted' })
		}
  })
}

exports.addDevice = function(req, res){
    var deviceState
    if(req.body.category == 'lamp')
        deviceState = 'false'
    else if(req.body.category == 'temp')
        deviceState = '20'
    
    var device = {
        name: req.body.name,
        category: req.body.category,
        img: req.body.img,
        watt: req.body.watt,
        model: req.body.model,
        state: deviceState,
        active: true
    }

    Room.findById(req.params.id).then(room => {
        room.devices.push(device)
        return room.save()
    }).then(room => {res.status(HttpStatus.CREATED).json(room)})
    .catch(err => res.status(HttpStatus.BAD_REQUEST).send(err))
}

exports.deleteDevice = function (req, res) {
    Room.findById(req.params.id).then(room => {
        const device = room.devices.id(req.params.deviceID)
        room.devices.pull(device)
        return room.save()
    }).then(room => {res.json(room)})
    .catch(err => res.status(HttpStatus.BAD_REQUEST).send(err))
}

exports.updateDeviceState = function (req, res) {
    Room.findById(req.params.id).then(room => {
        const device = room.devices.id(req.params.deviceID)
        device.state = req.body.state
        return room.save()
    }).then(room => {res.send(room)})
    .catch(err => res.status(HttpStatus.BAD_REQUEST).send(err))
}

exports.updateDevice = function (req, res) {
    Room.findById(req.params.id).then(room => {
        const device = room.devices.id(req.params.deviceID)
        device.name = req.body.name ? req.body.name : device.name
        device.img = req.body.img ? req.body.img : device.img
        return room.save()
    }).then(room => {res.status(HttpStatus.OK).send(room)})
    .catch(err => res.status(HttpStatus.BAD_REQUEST).send(err))
}

exports.moveDevice = function (req, res) {
    var device
    Room.findById(req.params.id).then(room => {
        device = room.devices.id(req.params.deviceID)
        room.devices.pull(device)
        return room.save()
    }).then(room => {
        Room.findById(req.body.newRoom).then(room => {
            room.devices.push(device)
            return room.save()
        }).then(room => {res.send(room)})
        .catch(err => res.status(HttpStatus.BAD_REQUEST).send(err))
    })
    .catch(err => res.status(HttpStatus.BAD_REQUEST).send(err))
}

exports.isDeviceActive = async function (req, res) {
    if( (await (Room.findOne({'devices.state': "true"}).exec())) != null)
        return true
    else 
        return false
}

exports.switchOffAllOnDevices = async function (req, res) {
    Room.find({})
    .then(rooms => {
        rooms.forEach(room => {
            room.devices.forEach(async device => {
                if(device.state == "true"){
                    device.state = "false"
                    await room.save()
                }
            })
        })
    })
    .then(dev => {
        res.status(HttpStatus.OK).send(dev)
    })
    .catch(err => res.status(HttpStatus.BAD_REQUEST).send(err))
}

exports.activateAlarm = function (req, res) {
    Room.findById(req.params.id).then(room => {
        room.alarm_state = true
        room.alarm_events.push({timestamp: Date.now()})
        return room.save()
    }).then(room => {
        axios.post("http://localhost:3000/api/notifications/command/system/broadcast",{
            "type": "alarm",
            "data": " ",
            "description": "Allarme ON: "+room.name
        }).then(response => {
            console.log(response)
            res.status(HttpStatus.OK).json(room)
        })
    })
    .catch(err => res.status(HttpStatus.BAD_REQUEST).send(err))
}

exports.deactivateAlarm = async function (req, res) {
    Room.findById(req.params.id).then(room => {
        room.alarm_state = false
        return room.save()
    }).then(room => {        
        axios.post("http://localhost:3000/api/notifications/command/system/broadcast",{
            "type": "alarm",
            "data": "",
            "description": "Allarme OFF: "+room.name
        }).then(response => {
            console.log(response)
            res.status(HttpStatus.CREATED).json(room)
        }).catch(err => console.log(err))
    }).catch(err => res.status(HttpStatus.BAD_REQUEST).send(err))
}