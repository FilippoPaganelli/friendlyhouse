const mongoose = require('mongoose')
const Notification = require("../models/notificationModel.js")(mongoose)
const userController = require('../controllers/usersController')
const HttpStatus = require('http-status-codes');

const supportedType = ['system', 'message', 'alarm', 'gps']
const defaultDescriptions = {
  'system': 'evento di sistema',
  'message' : 'hai un nuovo messaggio',
  'alarm': 'evento allarme',
  'gps': 'evento gps'
}

//verifica che il nome del token corrisponda con quello del token
exports.verifyUserParam = function(req, res, next) {
	if(req.params.username != req.user.username) return res.status(HttpStatus.UNAUTHORIZED).send("Access Denied")
  console.log("verify user")
	next()
}

//il destinatario della notifica deve essere il richiedente
exports.verifyOwenership = async function(req, res, next) {
  //recupera la notifica dato l'id.
  //se il destinatario corrisponde con il richiedente allora aggiunge al notifica alla richiesta
  //in questo modo non deve essere gettata dopo
  const notification = await Notification.findById(req.params.notificationid)
  if (notification.to == req.params.username) {
    req.notification = notification
    next()
  } else {
    return res.status(HttpStatus.UNAUTHORIZED).send("You don't own this notification")
  }
}

//internal use only, can be called only from within
exports.verifySystem = function(req, res, next) {
  if(req.ipInfo.ip != "127.0.0.1") return res.status(HttpStatus.UNAUTHORIZED).send("This api is intended for internal use only!" + req.ipInfo.ip)
  next()
}

/**
* Gets the notifications of the specified player
*/
exports.getNotifications = async function(req, res) {
  const who = req.params.username
  if (!(await userController.userExists(who))) return res.status(HttpStatus.CONFLICT).send("The specified user does not exists")

  Notification.find({to: who, dismissed: false}, function(err, result) {
		if (err) {
			res.send(err)
		} else {
			res.status(HttpStatus.OK).json(result)
		}
	})

}

/**
* Creates a new notification
*/
exports.createNotification = async function(req, res) {
  const from = req.params.username
  const to = req.body.to
  const type = req.body.type
  const data = req.body.data
  const description = req.body.description ?  req.body.description : defaultDescriptions[type]

  if (type == "system") return res.status(HttpStatus.UNAUTHORIZED).send("user can't create system notifification")
  if (!supportedType.includes(type)) return res.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).send('type not supported')
  if (!(await userController.userExists(from))) return res.status(HttpStatus.CONFLICT).send("Sender does not exists")
  if (!(await userController.userExists(to))) return res.status(HttpStatus.CONFLICT).send("Receiver does not exists")

  const notification = new Notification({
    from: from,
    to: to,
    type: type,
    description: description,
    data: data
  })

  try {
    const saveNotification = await notification.save()
    res.status(HttpStatus.CREATED).json(saveNotification)
  } catch (err) {
    console.log(err)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).send("Impossible to insert notification")
  }

}

/**
* Gets the specified notification
*/
exports.getNotification = function(req, res) {
  res.status(HttpStatus.OK).send(req.notification)
}

/**
* Marks as read the specified notification
*/
exports.dismissNotification = async function(req, res) {
  //console.log(req.notification)
  if (req.notification.dismissed == true) return res.status(HttpStatus.CONFLICT).send("Notification alredy dismissed")
  try {
    const notification = await Notification.findOneAndUpdate({
      _id: req.notification._id
    }, { dismissed: true });
    global.io.emit(`notification-dismissed`, notification.to)
    res.status(HttpStatus.OK).send(notification)
  } catch(err) {
    res.stauts(HttpStatus.INTERNAL_SERVER_ERROR)
  }
}

/**
* Creates a new system level notification for a specific player
*/
exports.createSystemNotification = async function(req, res) {
  console.log("Create system notification!")
  const from = req.body.from ? req.body.from : "system"
  const to = req.params.username
  const type = req.body.type ? req.body.type : "system"
  const data = req.body.data
  const description = req.body.description ?  req.body.description : defaultDescriptions[type]

  if (!(await userController.userExists(to))) return res.status(HttpStatus.CONFLICT).send("Receiver does not exists")

  const notification = new Notification({
    from: from,
    to: to,
    type: type,
    description: description,
    data: data
  })


  try {
    const saveNotification = await notification.save()
    global.io.emit(`event-${type}`, to)
    res.status(HttpStatus.CREATED).json(saveNotification)
  } catch (err) {
    console.log(err)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).send("Impossible to insert notification")
  }

}

/**
* Creates a new system level notification for every player
*/
exports.createBroadcast = async function(req, res) {

  console.log("broadcast called")

  const type = req.body.type ? req.body.type : "system"
  const data = req.body.data
  const description = req.body.description ?  req.body.description : defaultDescriptions[type]

  const userList = await userController.userList()
  try {
    for(const u of userList){
      var notification = new Notification({
        from: "system",
        to: u.username,
        type: type,
        description: description,
        data: data
      })
      var saveNotification = await notification.save()

      global.io.emit(`event-${type}`, u.username)

      console.log("saved", u.username)
    }
    console.log("all saved")
    res.status(HttpStatus.CREATED).send(userList)
  }catch(err) {
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).send("Impossible to insert notification")
  }


}

exports.readAllMessages = async function(req, res) {
  console.log("read all messages")
  const withWho = req.params.with
  const username = req.params.username
  try {
    const notification = await Notification.updateMany(
      {
        from: withWho,
        to: username,
        type: 'message'
      },
      { dismissed: true },
      { multi: 'true'}
    );
    global.io.emit(`notification-dismissed`, username)
    res.status(HttpStatus.OK).send()
  } catch(err) {
    res.stauts(HttpStatus.INTERNAL_SERVER_ERROR)
  }
}
