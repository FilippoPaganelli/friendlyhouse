const mongoose = require('mongoose')
const Feature = require("../models/featureModel.js")(mongoose)
const HttpStatus = require('http-status-codes')

exports.getUserFeature = async function(req, res) {
    Feature.find({title: req.params.feature, user: true}, function(err, feature){
        if(err)
            res.send(err)
        else {
            if(!feature) return res.status(HttpStatus.BAD_REQUEST).send("Feature does not exist.")
            res.json(feature)
        }
    })
}

exports.getOperatorFeature = async function(req, res) {
    Feature.find({title: req.params.feature, user: false}, function(err, feature){
        if(err)
            res.send(err)
        else {
            if(!feature) return res.status(HttpStatus.BAD_REQUEST).send("Feature does not exist.")
            res.json(feature)
        }
    })
}

exports.getFeatures = async function(req, res) {
    Feature.find({}).sort('priority').exec( function(err, features){
        if(err)
            res.send(err)
        else {
            if(!features) return res.status(HttpStatus.BAD_REQUEST).send("Features do not exist.")
            res.json(features)
        }
    })
}

exports.getUserFeatures = async function(req, res) {
    Feature.find({user:true}).sort('priority').exec( function(err, features){
        if(err)
            res.send(err)
        else {
            if(!features) return res.status(HttpStatus.BAD_REQUEST).send("Features do not exist.")
            res.json(features)
        }
    })
}

exports.getOperatorFeatures = async function(req, res) {
    Feature.find({user:false}).sort('priority').exec( function(err, features){
        if(err)
            res.send(err)
        else {
            if(!features) return res.status(HttpStatus.BAD_REQUEST).send("Features do not exist.")
            res.json(features)
        }
    })
}