const mongoose = require('mongoose');
const User = require("../models/usersModel.js")(mongoose);
const { registerValidation, loginValidation } = require("../validation/userValidation")
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

exports.show_index = function(req, res) {
	res.sendFile(appRoot  + '/www/index.html');
};

exports.userExists = async function(user) {
	if (await User.findOne({username: user})) return true; else return false
}

exports.userList = async function() {
  return await User.find({}, {password:0})
}

exports.registerUser =  async function (req, res) {
	const { error } = registerValidation(req.body)
	if (error) return res.status(400).send(error.details[0].message)

	//hash password
	const salt = await bcrypt.genSalt(10)
	const hashPassword = await bcrypt.hash(req.body.password, salt)

	const user = new User({
		username: req.body.username,
		password: hashPassword,
		isAdmin: req.body.role === 'admin' ? true : false
	})
	try {
		const savedUser = await user.save()
		res.status(201).send({user: user._id})
	}catch (err) {
		res.status(400).send("Error while saving")
	}
}

exports.login = async function (req, res) {
	//validate body
	const { error } = loginValidation(req.body)
	if (error) return res.status(400).send(error.details[0].message)

	//check if the user exist
	const user = await User.findOne({username: req.body.username})
	if (!user) return res.status(400).send("Username does not exist.")

	//validate password
	const validPass = await bcrypt.compare(req.body.password, user.password)
	if (!validPass) res.status(400).send("Wrong password.")

	//create and sign a token
	const token = jwt.sign(
		{_id: user._id, username: user.username, isAdmin: user.isAdmin},
		process.env.TOKEN_SECRET,
		{ expiresIn: '12h' }
	)
	res.status(200).header('auth-token', token).send(token)
}

exports.listUsers = async function (req, res) {
	console.log("list users called")
	User.find({}, {password: 0, __v:0}, function(err, result) {
		if (err) {res.send(err);}
		else {res.json(result);}
	});
}

exports.getUser = async function(req, res) {
	res.status(200).json("ok")
}

exports.deleteUser = function(req, res) {
	User.deleteOne({username: req.params.username}, function(err, result) {
		if (err)
			res.send(err);
		else{
			if(result.deletedCount==0){
				res.status(404).send();
			}
			else{
				res.status(204).send();
			}
		}
  });
};

exports.toAdmin = function (req, res) {
	console.log("toAdmin")
	console.log(req.params.username)
	User.findOneAndUpdate({username: req.params.username}, {isAdmin: true}, function(err, user) {
		if (err)
			res.send(err);
		else{
			if(user==null){
				res.status(404).send();
			}
			else{
				user.isAdmin = true
				res.status(200).json(user);
			}
		}
	});
}

exports.toUser = function (req, res) {
	User.findOneAndUpdate({username: req.params.username}, {isAdmin: false}, function(err, user) {
		if (err)
			res.send(err);
		else{
			if(user==null){
				res.status(404).send({
					description: 'user not found'
				});
			}
			else{
				user.isAdmin = false
				res.status(200).json(user);
			}
		}
	});
}


exports.show_index = function(req, res) {
	res.sendFile(appRoot  + '/www/index.html');
};
