const mongoose = require('mongoose')
const Message = require("../models/messageModel.js")(mongoose)
const userController = require('../controllers/usersController')
const HttpStatus = require('http-status-codes');
const axios = require('axios')

exports.verifyCurrentUserBody = function(req, res, next) {
	if(req.user.username != req.body.from) return res.status(401).send("Access Denied")
	next()
}

exports.verifyCurrentUserParam = function(req, res, next) {
	if(req.user.username != req.params.from) return res.status(401).send("Access Denied")
	next()
}

exports.insertMessage = async function(req, res) {
	const userExist = await userController.userExists(req.body.to)
  const from = req.body.from
  const to = req.body.to
  const content = req.body.content
	if (!userExist) res.status(HttpStatus.CONFLICT).send("Receiver does not exists")
	if (req.body.from == req.body.to) res.status(HttpStatus.CONFLICT).send("Can't send a message to yourself!")

	const message = new Message({
		from: from,
		to: to,
		content: content,
	})
	try {
		const saveMessage = await message.save()

    axios
      .post(`http://localhost:3000/api/notifications/command/system/${to}`,{"type": "message", "from":from})
      .then(response => {
        res.status(HttpStatus.CREATED).json(saveMessage)
      }).catch(err => {
        console.log("error", err)
      })

	} catch (err) {
		res.status(400)
	}

}

exports.getMessages = async function(req, res) {
	const from = req.params.from
	const to = req.params.to

	if (! await userController.userExists(from)) return res.status(HttpStatus.CONFLICT).send("Sender does not exists")
	if (! await userController.userExists(to)) return res.status(HttpStatus.CONFLICT).send("Receiver does not exists")

	Message.find({from: from, to: to}, function(err, result) {
		if (err) {
			res.send(err)
		} else {
			res.status(HttpStatus.OK).json(result)
		}

	})
}

exports.getChat = async function(req, res) {
	console.log("get chat")
	const from = req.params.from
	const to = req.params.to

	if (! await userController.userExists(from)) return res.status(HttpStatus.CONFLICT).send("Sender does not exists")
	if (! await userController.userExists(to)) return res.status(HttpStatus.CONFLICT).send("Receiver does not exists")

	Message.find({$or: [{from: from, to: to}, {from: to, to: from}]})
		.sort({'timestamp':'asc'})
		.exec(function(err, result) {
			if (err) {
				res.send(err)
			} else {
				res.status(HttpStatus.OK).json(result)
			}
	})
}
