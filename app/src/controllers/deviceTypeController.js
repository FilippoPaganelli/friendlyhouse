const mongoose = require('mongoose')
const DeviceType = require("../models/deviceTypeModel.js")(mongoose)
const HttpStatus = require('http-status-codes')

exports.show_index = function(req, res) {
	res.sendFile(appRoot  + '/www/index.html')
}

exports.getDeviceTypes = async function(req, res) {
    DeviceType.find({}, function(err, types) {
        if(err)
            res.send(err)
        else {
            if(!types) return res.status(HttpStatus.BAD_REQUEST).send("There are no device types")
            res.json(types)
        }
    })
}

exports.getDeviceTypeByModel = async function(req, res) {
    DeviceType.find({model: req.params.model}, function(err, type){
        if(err)
            res.send(err)
        else {
            if(!type) return res.status(HttpStatus.BAD_REQUEST).send("There is no such device type")
            res.json(type)
        }
    })
}

exports.insertNewType = async function(req, res){
    const type = new DeviceType({
        name: req.body.name,
        model: req.body.model,
        watt: req.body.watt,
        description: req.body.description,
        category: req.body.category
    })

    try{
        const savedType = await type.save()
        res.send({type: type._id})
    }catch(err){
        res.status(HttpStatus.BAD_REQUEST).send(err)
    }
}

exports.deleteType = function(req, res){
    DeviceType.deleteOne({model: req.params.model}, function(err,result){
        if (err)
            res.send(err)
        else {
            if(result.deletedCount == 0)
                res.status(HttpStatus.NOT_FOUND).send({
                    description: 'Type not found'
                })
            else
                res.json({ message: 'Type successfully deleted' })
        }
    })
}