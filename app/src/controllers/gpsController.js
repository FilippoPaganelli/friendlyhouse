const mongoose = require('mongoose')
const GPS = require("../models/gpsModel.js")(mongoose)
const axios = require('axios')
const HttpStatus = require('http-status-codes')
const roomController = require('../controllers/roomsController')

exports.show_index = function(req, res) {
	res.sendFile(appRoot  + '/www/index.html')
}

exports.addUserPosition = async function (req, res){
	const gps = new GPS({
		user: req.params.user,
		home_pos: req.body.home_pos,
		latitude: req.body.latitude,
		longitude: req.body.longitude
	})
	try{
		const savedGPS = await gps.save()
		let description
		if(gps.home_pos == "far")
			description = "ti stai allontanando dall'abitazione.. "
		else if(gps.home_pos == "near")
			description = "ti stai avvicinando dall'abitazione.. "
			
		var isActive = 'near'
		var data = await roomController.isDeviceActive()
		console.log("AAAA "+data)
		if((data == true) && (gps.home_pos == 'far'))
			isActive = 'far'


		axios.post("http://localhost:3000/api/notifications/command/system/"+req.params.user,{
			"type": "gps",
			"data": isActive,
			"description": description
		}).then(response => {
			res.status(HttpStatus.CREATED).json(gps)
		})

	}catch(err){
		res.status(HttpStatus.BAD_REQUEST).send(err)
	}
}

exports.getUserPositions = async function (req, res){
	GPS.find({user: req.params.user}, function(err, gps){
		if(err)
			res.send(err)
		else {
			if(!gps) return res.status(HttpStatus.NOT_FOUND).send("There are no locations for this user")
			res.json(gps)
		}
	})
}
