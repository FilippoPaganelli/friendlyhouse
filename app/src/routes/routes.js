//questo file collega gli indirizzi con i metodi del controller
module.exports = function(app) {
	const userController = require('../controllers/usersController')
	const featureController = require('../controllers/featureController')
	const roomController = require('../controllers/roomsController')
	const chatController = require('../controllers/chatController')
	const deviceTypeController = require('../controllers/deviceTypeController')
	const deviceActivityLogController = require('../controllers/deviceActivityLogController')
	const gpsController = require('../controllers/gpsController')
	const notificationController = require('../controllers/notificationController')
	const alarmController = require('../controllers/alarmController')
  const elettricController = require('../controllers/elettricController')

	const verify = require('../auth/verifyToken')

	///////////////////////////
	// USER ROUTES
	///////////////////////////
	app.route('/api/user/register')
		.post(userController.registerUser)
	app.route('/api/user/login')
		.post(userController.login)

	app.route('/api/users')
		.get(
			verify.verifyToken,
			userController.listUsers
		)
	app.route('/api/users/:username')
		.delete(
			verify.verifyToken,
			verify.verifyAdmin,
			userController.deleteUser
		)
	app.route('/api/users/:username/toadmin')
		.put(
			verify.verifyToken,
			verify.verifyAdmin,
			userController.toAdmin
		)

	app.route('/api/users/:username/touser')
		.put(
			verify.verifyToken,
			verify.verifyAdmin,
			userController.toUser
		)

	app.route('/api/user/testtoken')
		.get(
			verify.verifyToken,
			userController.getUser
		)

	app.route('/api/user/testtokenAdmin')
		.get(
			verify.verifyToken,
			verify.verifyAdmin,
			userController.getUser
		)

	///////////////////////////
	// FEATURE ROUTES
	///////////////////////////
	app.route('/api/features/user')
		.get(featureController.getUserFeatures)

	app.route('/api/features/operator')
		.get(verify.verifyToken,
			verify.verifyAdmin,
			featureController.getOperatorFeatures)

	app.route('/api/features/user/:feature')
		.get(featureController.getUserFeature)

	app.route('/api/features/operator/:feature')
		.get(verify.verifyToken,
			verify.verifyAdmin,
			featureController.getOperatorFeature)

	app.route('/api/features')
		.get(featureController.getFeatures)


	///////////////////////////
	// ROOMS ROUTES
	///////////////////////////
	app.route('/api/rooms')
		.get(roomController.getUserRooms)

	app.route('/api/rooms/:name')
		.get(roomController.getUserRoom)

	app.route('/api/rooms/addRoom')
		.post(verify.verifyToken,
			verify.verifyAdmin,
			roomController.insertNewRoom)

	app.route('/api/rooms/updateRoom/:id')
		.put(verify.verifyToken,
			verify.verifyAdmin,
			roomController.updateRoom)

	app.route('/api/rooms/deleteRoom/:id')
		.delete(verify.verifyToken,
			verify.verifyAdmin,
			roomController.deleteRoom)

	///////////////////////////
	// DEVICE ROUTES
	///////////////////////////
	app.route('/api/rooms/:id/addDevice')
		.put(verify.verifyToken,
			verify.verifyAdmin,
			roomController.addDevice)

	app.route('/api/rooms/:id/deleteDevice/:deviceID')
		.put(verify.verifyToken,
			verify.verifyAdmin,
			roomController.deleteDevice)

	app.route('/api/rooms/:id/updateDeviceState/:deviceID')
		.put(verify.verifyToken,
			roomController.updateDeviceState)

	app.route('/api/rooms/:id/updateDevice/:deviceID')
		.put(verify.verifyToken,
			verify.verifyAdmin,
			roomController.updateDevice)

	app.route('/api/rooms/:id/moveDevice/:deviceID')
		.put(verify.verifyToken,
			verify.verifyAdmin,
			roomController.moveDevice)

	app.route('/api/rooms/switchAllOff')
		.put(verify.verifyToken,
			roomController.switchOffAllOnDevices)

	///////////////////////////
	// DEVICE TYPES ROUTES
	///////////////////////////
	app.route('/api/deviceType')
		.get(deviceTypeController.getDeviceTypes)

	app.route('/api/deviceType/:model')
		.get(deviceTypeController.getDeviceTypeByModel)

	app.route('/api/deviceType/newModel')
		.post(verify.verifyToken,
			verify.verifyAdmin,
			deviceTypeController.insertNewType)

	app.route('/api/deviceType/deleteModel/:model')
		.delete(verify.verifyToken,
			verify.verifyAdmin,
			deviceTypeController.deleteType)

	///////////////////////////
	// DEVICE ACTIVITIES LOGS ROUTES
	///////////////////////////
	app.route('/api/deviceLog/all/:deviceID')
		.get(
      //verify.verifyToken,
			deviceActivityLogController.getLastDeviceActivityLog)

	app.route('/api/deviceLog/:deviceID')
		.get(verify.verifyToken,
			deviceActivityLogController.getLastDeviceActivityLog)

	app.route('/api/deviceLog/addLog/:deviceID')
		.post(verify.verifyToken,
			deviceActivityLogController.addDeviceActivityLog)

	///////////////////////////
	// GPS LOGS ROUTES
	///////////////////////////
	app.route('/api/gps/:user')
		.put(verify.verifyToken,
			gpsController.addUserPosition)

	app.route('/api/gps/:user')
		.get(verify.verifyToken,
			gpsController.getUserPositions)


	///////////////////////////
	// CHAT ROUTES
	///////////////////////////
	app.route('/api/chat/message')
		.post(
			verify.verifyToken,
			chatController.verifyCurrentUserBody,
			chatController.insertMessage
			)

	app.route('/api/chat/message/:from/to/:to')
		.get(
			verify.verifyToken,
			chatController.verifyCurrentUserParam,
			chatController.getMessages
			)

	app.route('/api/chat/message/:from/with/:to')
		.get(
			verify.verifyToken,
			chatController.verifyCurrentUserParam,
			chatController.getChat
			)

	///////////////////////////
	// NOTIFICATION ROUTES
	///////////////////////////
	app.route('/api/notifications/users/:username')
		.get(
		verify.verifyToken,
		notificationController.verifyUserParam,
		notificationController.getNotifications
		)
		//va anche bene che un utente possa mandare in modo diretto
		//delle notifiche
		.post(
		verify.verifyToken,
		notificationController.verifyUserParam,
		notificationController.createNotification
		)
	app.route('/api/notifications/users/:username/:notificationid')
		.get(
			verify.verifyToken,
			notificationController.verifyUserParam,
			notificationController.verifyOwenership,
			notificationController.getNotification
		)
		.put(
		verify.verifyToken,
		notificationController.verifyUserParam,
		notificationController.verifyOwenership,
		notificationController.dismissNotification
		)

	app.route('/api/notifications/command/system/broadcast')
		.post(
		notificationController.verifySystem,
		notificationController.createBroadcast
		)

	app.route('/api/notifications/command/system/:username')
		.post(
		notificationController.verifySystem,
		notificationController.createSystemNotification
		)

	app.route('/api/notifications/command/users/:username/readmessages/:with')
		.put(
		verify.verifyToken,
		notificationController.verifyUserParam,
		notificationController.readAllMessages
		)

	///////////////////////////
	// ALARM ROUTES
	///////////////////////////
	app.route('/api/alarm/activate/:id')
		.put(roomController.activateAlarm)

	app.route('/api/alarm/deactivate/:id')
		.put(roomController.deactivateAlarm)

	app.route('/api/alarm/verify')
		.post(verify.verifyToken,
			alarmController.verify)

	app.route('/api/alarm/update')
		.put(verify.verifyToken,
			verify.verifyAdmin,
			alarmController.update)

	app.route('/api/alarm/add')
		.post(verify.verifyToken,
			verify.verifyAdmin,
			alarmController.add)


	///////////////////////////
	// ELETRIC ROUTES
	///////////////////////////
  app.route('/api/eletric/global/:from/:to')
    .get(
      //verify.verifyToken,
      elettricController.global
    )

	app.use(userController.show_index)
}
