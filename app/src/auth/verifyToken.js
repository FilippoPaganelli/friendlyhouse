const jwt = require('jsonwebtoken')

/*
La prima funzione verifica la presenza del token e la sua correttezza
mentre la seconda verifica se l'utente attuale ha i permessi di amministratore
*/

exports.verifyToken = function(req, res, next) {
	const token = req.header('auth-token')
	if (!token) return res.status(401).send("Access Denied")

	try {
		const verified = jwt.verify(token, process.env.TOKEN_SECRET)
		req.user = verified
		next()
	} catch (err) {
		res.status(400).send("Invalid Token")
	}
}

exports.verifyAdmin = function (req, res, next) {
	if (!req.user.isAdmin) return res.status(401).send("Access Denied")
	next()
}