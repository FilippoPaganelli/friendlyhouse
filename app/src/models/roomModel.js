module.exports = function(mongoose){
    var Schema = mongoose.Schema
    var RoomModel = new mongoose.Schema({
        name: {
            type: String,
            required: true
        },
        img: {
            type: String,
            required: true
        },
        link: {
            type: String,
            required: true
        },
        alarm_state: {
            type: Boolean,
            required: false
        },
        alarm_events: {
            type: [{
                timestamp: {
                    type: Date,
                    default: Date.now,
                    required: true
                }
            }],
            required: false
        },
        info:{
            type: String,
            required: false
        },
        description: {
            type: String,
            required: true
        },
        devices:{
            type: [{
                name: {
                    type: String,
                    required: true
                },
                category:{
                    type: String,
                    required: true
                },
                img: {
                    type: String,
                    required: true
                },
                model: {
                    type: String,
                    required: true
                },
                watt: {
                    type: Number,
                    required: true
                },
                state: {
                    type: String,
                    required: true
                },
                active: {
                    type: Boolean,
                    required: true
                }
            }],
            required: true
        }
    })

    return mongoose.model('roomsmodel', RoomModel, 'Rooms')
}