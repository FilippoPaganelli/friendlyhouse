module.exports = function(mongoose){
    var Schema = mongoose.Schema
    var NotificationSchema = new mongoose.Schema({
        from: {
            type: String,
            required: true
        },
        to: {
            type: String,
            required: true
        },
        type: {
            type: String,
            required: true
        },
        dismissed: {
            type: Boolean,
            default: false,
            required: true
        },
        description: {
          type: String,
          required: true
        },
        data: {
          type: String,
        },
        timestamp: {
            type: Date,
            default: Date.now,
            required: true
        }
    })

    return mongoose.model('notificationmodel', NotificationSchema, 'Notifications')
}
