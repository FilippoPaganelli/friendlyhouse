module.exports = function(mongoose){
    var Schema = mongoose.Schema
    var DeviceTypeModel = new mongoose.Schema({
        name: {
            type: String,
            required: true
        },
        model: {
            type: String,
            required: true
        },
        watt: {
            type: Number,
            required: true
        },
        description: {
            type: String,
            required: false
        },
        category: {
            type: String,
            required: true
        }
    })

    return mongoose.model('devicetypemodel', DeviceTypeModel, 'DeviceType')
}