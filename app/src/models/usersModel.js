module.exports = function(mongoose) {
	var Schema = mongoose.Schema
	var UserSchema = new mongoose.Schema({
		username: {
			type: String,
			required: true
		},
		password: {
			type: String,
			required: true,
			min: 6,
			max: 1024
		},
		isAdmin: {
			type: Boolean,
			required: true
		}
	})
	return mongoose.model('usermodel', UserSchema, 'Users');
}