module.exports = function(mongoose){
    var Schema = mongoose.Schema
    var FeatureModel = new mongoose.Schema({
        title: {
            type: String,
            required: true
        },
        user: {
            type: Boolean,
            required: true
        },
        priority: {
            type: Number,
            required: true
        },
        icon: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        img: {
            type: String,
            required: true
        },
        link: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        }
    })

    return mongoose.model('featuremodel', FeatureModel, 'Features')
}