module.exports = function(mongoose){
    var Schema = mongoose.Schema
    var AlarmModel = new mongoose.Schema({
        pin: {
            type: String,
            required: true
        },
        timestamp: {
            type: Date,
            default: Date.now,
            required: true
        }
    })

    return mongoose.model('alarmmodel', AlarmModel, 'Alarm')
}