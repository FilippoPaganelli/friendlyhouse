module.exports = function(mongoose){
    var Schema = mongoose.Schema
    var GPSModel = new mongoose.Schema({
        user: {
            type: String,
            required: true
        },
        home_pos:{
            type: String,
            required: true
        },
        latitude: {
            type: Number,
            required: true
        },
        longitude: {
            type: Number,
            required: true
        }
    })
    return mongoose.model('gpsmodel', GPSModel, 'GPS')
}
