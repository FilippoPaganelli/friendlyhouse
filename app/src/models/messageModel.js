module.exports = function(mongoose) {
	var Schema = mongoose.Schema
	var MessageSchema = new mongoose.Schema({
		from: {
			type: String,
			required: true
		},
		to: {
			type: String,
			required: true,
		},
		content: {
			type: String,
			required: true
		},
		timestamp: {
			type: Date,
			required: true,
			default: Date.now
		}
	})
	return mongoose.model('messagemodel', MessageSchema, 'Messages');
}