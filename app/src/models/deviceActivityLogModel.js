module.exports = function(mongoose){
    var Schema = mongoose.Schema
    var DeviceActivityLogModel = new mongoose.Schema({
        user: {
            type: String,
            required: true
        },
        device: {
            type: Schema.Types.ObjectId,
            required: true
        },
        device_model: {
          type: String,
          required: true
        },
        new_state: {
            type: String,
            required: true
        },
        moved_to: {
            type: Schema.Types.ObjectId,
            required: true
        },
        timestamp: {
            type: Date,
            default: Date.now,
            required: true
        }
    })

    return mongoose.model('deviceactivitylogmodel', DeviceActivityLogModel, 'DeviceActivityLogs')
}
