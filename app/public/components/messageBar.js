const MessageBar = {
	data: function () {
		return {
			snackbar: false,
			message: '',
			color: ''
		}
	},
	template: `
		<v-snackbar v-model="snackbar" :timeout="4000" top :color="color">
			<span> {{message}} </span>
			<template v-slot:action="{ attrs }">
				<v-btn dark text v-bind="attrs" @click="snackbar = false">
					Close
				</v-btn>
			</template>
		</v-snackbar>
	`,
	methods: {
		success(msg) {
			this.message = msg
			this.color = "success"
			this.snackbar = true
		},
		info(msg) {
			this.message = msg
			this.color = "info"
			this.snackbar = true
		},
		error(msg) {
			this.message = msg
			this.color = "error"
			this.snackbar = true
		}
	}
}



/**
	ORA IL COMPONENTE VIENE CARICATO
	FARE IN MODO CHE FUNZIONI COME MESASGE BAR


const MessageBar = {
	template: `
		<p>text</p>
	`
}

*/