const NotificationManager = {
    data: () => ({
        socket: null
    }),
    computed: {
        eventBus() {
          return this.$store.getters.getEventBus
        },
        username() {
          return this.$store.getters.getUsername
        }
    },
    methods: {
      init: function() {
        this.socket = io()

        this.socket.on("event-message", to => {
          if (to == this.username || to == "") {
            this.eventBus.$emit('ResponseReceived', "")
            this.eventBus.$emit('ReloadNotifications', "")
          }
        })

        this.socket.on("notification-dismissed", to => {
          if (to == this.username || to == "") {
            this.eventBus.$emit('ReloadNotifications', "")
          }
        })

        this.socket.on("event-alarm", to => {
          if (to == this.username || to == "") {
            this.eventBus.$emit('ReloadNotifications', "")
          }
        })

        this.socket.on("event-gps", to => {
          if (to == this.username || to == "") {
            this.eventBus.$emit('ReloadNotifications', "")
          }
        })

      }
    },
    mounted(){
        this.init()
    },
    template: '<div></div>'
}
