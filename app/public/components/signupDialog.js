const Signup = {
	components: {
		'message-bar': MessageBar
	},
	template: `
	<v-app>
		<v-dialog v-model="dialog" persistent max-width="600px">
			<message-bar ref="messageBar"></message-bar>
			<v-card>
				<v-card-title>
					<span class="headline">Creazione nuovo utente</span>
				</v-card-title>
				<v-card-text>

					<v-form
						ref="form"
						lazy-validation
					>
						<v-text-field
							label="Username"
							prepend-icon="mdi-account-circle"
							v-model="username"
							:rules="usernameRules"
							required
						/>
						<v-text-field
							:type="showPassword ? 'text': 'password' "
							label="Password"
							prepend-icon="mdi-lock"
							:append-icon="showPassword ? 'mdi-eye' : 'mdi-eye-off'"
							@click:append="showPassword = !showPassword"
							v-model="password"
							:rules="passwordRules"
							required
						/>
					</v-form>
				</v-card-text>

				<v-card-actions>
					<v-spacer></v-spacer>
						<v-btn color="blue darken-1" text @click="dialog = false">Chiudi</v-btn>
						<v-btn color="blue darken-1" text @click="handleSubmit">Registra</v-btn>
				</v-card-actions>
			</v-card>
		</v-dialog>
	</v-app>
	`,
	data() {
		return {
			username: "",
			usernameRules: [
		        v => !!v || 'Name is required',
		    ],
			password: "",
			passwordRules: [
				v => !!v || 'Password is required',
				v => (v && v.length >= 8) || 'Password must be less than 8 characters',
			],
			role: 'user',
			showPassword: false,
			snackbar: false,
			snackbarMessage: '',
			snackbarColor: '',
			dialog: false
		}
	},
	methods: {
		resetValidation() {
			this.$refs.form.resetValidation()
		},
		handleSubmit() {
			dialog: false
			if (this.$refs.form.validate()) {
				console.log("isValid")
				//make a submit

				axios.post("http://192.168.1.209:3000/api/user/register", {
					username: this.username,
					password: this.password,
					role: this.role
				}).then(response => {
					console.log(response)
					console.log("ok")
					this.$refs.messageBar.success("Utente registrato!")
				}).catch(error => {
					(console.log(error.response.data))
					this.$refs.messageBar.error(error.response.data)
				});

			} else {
				console.log("is not valid")
			}
		},
		show() {
			dialog: true
		},
		init() {
			this.resetValidation()
		}
	},
	mounted(){
		this.init()
	}
}