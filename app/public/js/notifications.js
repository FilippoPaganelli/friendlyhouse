const MsgNotification = {
  props: ['notification'],
  template: `
    <v-list-item
      :key='notification._id'
    >
      <v-row>
        <v-col
          sm=1
          cols=2
        >
          <v-list-item-icon>
            <v-icon>mdi-message</v-icon>
          </v-list-item-icon>
        </v-col>

        <v-col
          sm=8
          cols=10
        >
          <v-list-item-content>
            {{notification.from}} ti ha mandato un messaggio
          </v-list-item-content>
        </v-col>





        <v-col
          sm=2
          cols=6
        >
          <v-btn
            @click="openChat"
            outlined
          >
            Apri chat
          </v-btn>
        </v-col>
        <v-col
          sm=1
          cols=6
        >
          <v-btn
            @click="deleteNotification"
            icon
            color="red"
          >
            <v-icon>mdi-delete</v-icon>
          </v-btn>
        </v-col>

      </v-row>
    </v-list-item>
  `,
  methods: {
    openChat: function() {
      router.push(`/chat/${this.notification.from}`)
    },
    deleteNotification: function() {
      axios
      .put(
          `http://${this.serverIP}:3000/api/notifications/users/${this.notification.to}/${this.notification._id}`,
          {},
          {headers: {
              'auth-token': localStorage.getItem('auth-token')
          }})
      .then(response => console.log(response))
    }
  }
}

const AlarmNotification = {
  props: ['notification'],
  template: `
    <v-list-item
      :key='notification._id'
    >
      <v-row>
        <v-col
          sm=1
          cols=2
        >
          <v-list-item-icon>
            <v-icon>mdi-alarm-light</v-icon>
          </v-list-item-icon>
        </v-col>

        <v-col
          sm=8
          cols=10
        >
          <v-list-item-content>
            {{notification.description}}
          </v-list-item-content>
        </v-col>

        <v-col
          sm=2
          cols=6
        >
          <v-btn
            @click="openAlarm"
            outlined
          >
            Apri Allarme
          </v-btn>
        </v-col>


        <v-col
          sm=2
          cols=6
        >
          <v-btn
            @click="deleteNotification"
            icon
            color="red"
          >
            <v-icon>mdi-delete</v-icon>
          </v-btn>
        </v-col>

      </v-row>
    </v-list-item>
  `,
  methods: {
    openAlarm: function() {
      router.push(`/alarm`)
    },
    deleteNotification: function() {
      axios
      .put(
          `http://${this.serverIP}:3000/api/notifications/users/${this.notification.to}/${this.notification._id}`,
          {},
          {headers: {
              'auth-token': localStorage.getItem('auth-token')
          }})
      .then(response => console.log(response))
    }
  }
}

const GpsNotification = {
  props: ['notification'],
  template: `
    <v-list-item
      :key='notification._id'
    >
      <v-list-item-icon>
        <v-icon>mdi-navigation</v-icon>
      </v-list-item-icon>

      <v-list-item-content>
        <v-list-item-title>{{notification.description}}</v-list-item-title>
      </v-list-item-content>

      <v-spacer></v-spacer>

      <v-btn
      class="mx-3"
      v-if="notification.data == 'far'"
      @click="switchOffAllOnDevices"
      outlined
      >
        Spegni tutto
      </v-btn>

      <v-btn
        @click="openRoomsDevices"
        outlined
      >
        Controlla Dispositivi
      </v-btn>

      <v-btn
        @click="deleteNotification"
        icon
        color="red"
      >
        <v-icon>mdi-delete</v-icon>
      </v-btn>
    </v-list-item>
  `,
  methods: {
    openRoomsDevices: function() {
      router.push(`/roomsdevices`)
    },
    switchOffAllOnDevices: function () {
      axios
      .put(
          'http://'+this.serverIP+':3000/api/rooms/switchAllOff',
          {},
          {headers: {
              'auth-token': localStorage.getItem('auth-token')
          }})
      .then(response => console.log(response))
    },
    deleteNotification: function() {
      axios
      .put(
          `http://${this.serverIP}:3000/api/notifications/users/${this.notification.to}/${this.notification._id}`,
          {},
          {headers: {
              'auth-token': localStorage.getItem('auth-token')
          }})
      .then(response => console.log(response))
    }
  }
}

const Notifications = {
  components: {
      'msg-notification': MsgNotification,
      'alarm-notification': AlarmNotification,
      'gps-notification': GpsNotification
  },
  data() {
    return {
      notifications: []
    }
  },
  computed: {
      username() {
        return this.$store.getters.getUsername;
      },
      eventBus() {
        return this.$store.getters.getEventBus
      }
  },
  methods: {
    loadNotifications: function() {
      axios
        .get(
            `http://${this.serverIP}:3000/api/notifications/users/${this.username}`,
            {headers: {
                'auth-token': localStorage.getItem('auth-token')
            }})
        .then(response => {
          console.log(response.data)
          this.notifications = response.data
        })

    },
    init: function() {
      this.eventBus.$on('ReloadNotifications', this.loadNotifications);
      this.loadNotifications()
    }
  },
  mounted() {
    this.eventBus.$emit('updateBreadcrumb', [
      {
          text: 'Home',
          disabled: false,
          href: '/home'
      },{text: '/'},{
          text: 'Notifiche',
          disabled: false,
          href: '/notifications'
      }])
    this.init()
  },
  beforeRouteLeave(to, from, next) {
    this.eventBus.$off('ReloadNotifications', this.loadNotifications)
    next()
  },
  template: `
    <v-container elevation-12 white mt-5>
      <v-list>
          <template v-for="notification in notifications">
            <msg-notification v-if="notification.type == 'message'" :notification="notification"></msg-notification>
            <alarm-notification v-if="notification.type == 'alarm'" :notification="notification"></alarm-notification>
            <gps-notification v-if="notification.type == 'gps'" :notification="notification"></gps-notification>
          </template>
       </v-list>
    </v-container>
  `
}
