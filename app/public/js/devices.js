const UpdateDeviceDialog = {
    props: ['device', 'roomID'],
    data: () => ({
        rooms: [],
        roomsName: [],
        interact: false,
        choosenRoom: null,
        dialog: false,
        name: "",
        nameRules: [v => !!v || 'Name is required'],
        img: "",
        imgRules: [v => !!v || 'URL is required']
    }),
    computed: {
        eventBus() {
            return this.$store.getters.getEventBus
        }
    },
    methods: {
        updateDevice(){
            this.dialog = false
            axios.put("http://"+this.serverIP+":3000/api/rooms/"+this.roomID+"/updateDevice/"+this.device._id, {
                name: this.name,
                img: this.img
            },{headers: {
              'auth-token': localStorage.getItem('auth-token')
            }}).then(response => {
                this.eventBus.$emit("deviceUpdated", "")
            }).catch(error => {
                console.log(error)
            })

            // qui spostare il dispositivo nella nuova stanza se scelta
            if(this.choosenRoom != null){
                var id
                this.rooms.forEach(room => {
                    if(room.name == this.choosenRoom)
                        id = room._id
                    console.log("ROOM: "+room.name+" ID: "+room._id)
                })
                axios.put("http://"+this.serverIP+":3000/api/rooms/"+this.roomID+"/moveDevice/"+this.device._id, {
                  newRoom: id
                },{headers: {
                  'auth-token': localStorage.getItem('auth-token')
                }}).then(response => {
                    this.eventBus.$emit("deviceUpdated", "")
                }).catch(error => {
                    console.log(error)
                })
            }
        },
        initRooms(){
            axios.get("http://"+this.serverIP+":3000/api/rooms")
            .then(response => {
                this.rooms = response.data
                response.data.forEach(room => {this.roomsName.push(room.name)})
            }).catch(error => (this.$refs.messagebar.error(error.response.data)))
        }
    },
    mounted(){
        this.initRooms()
    },
    template: `
    <v-row justify="center">
    <v-dialog v-model="dialog" persistent max-width="600px">
      <template v-slot:activator="{ on, attrs }">
        <v-btn color="blue lighten-2" text @click="interact = true" v-bind="attrs" v-on="on">
            MODIFICA
        </v-btn>
      </template>
      <v-card>
        <v-card-title>
          <span class="headline">Modifica Dispositivo</span>
        </v-card-title>
        <v-card-text>
          <v-container>
          <v-form
          ref="form"
          lazy-validation
          >
            <v-row>
              <v-col cols="12" sm="12" md="12">
                <v-text-field
                :label="device.name"
                required
                hint="Inserisci il nuovo nome del dispositivo"
                persistent-hint
                :rules="nameRules"
                v-model="name"
                ></v-text-field>
              </v-col>
              <v-col cols="12" sm="12" md="12">
                <v-text-field
                :label="device.img"
                required
                hint="Inserisci l'URL della nuova immagine per il tuo dispositivo"
                persistent-hint
                :rules="imgRules"
                v-model="img"
                ></v-text-field>
              </v-col>
            </v-row>
            </v-form>
          </v-container>
          <v-container>
            <v-form v-if="interact == true">
            <v-row>
                <v-col cols="6" sm="6" md="6">
                <span>
                    Se vuoi spostare il dispositivo seleziona la nuova stanza
                </span>
                </v-col>
                <v-col cols="6" sm="6" md="6">
                    <v-select
                    v-model="choosenRoom"
                    :items="roomsName"
                    label="Nuova Stanza"
                ></v-select>
                </v-col>
            </v-row>
            </v-form>
          </v-container>
          <small>*indicates required field</small>
        </v-card-text>
        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="blue darken-1" text @click="dialog = false">Chiudi</v-btn>
          <v-btn color="blue darken-1" text @click="updateDevice">Salva</v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
  </v-row>`
}

const DeleteDeviceDialog = {
    props: ['device', 'roomID', 'dummyID'],
    data: () => ({
        deletedev: false,
        dialog: false
    }),
    computed: {
        eventBus() {
          return this.$store.getters.getEventBus
        }
    },
    methods: {
        deleteOrDisableDevice(){
            this.dialog = false
            if(this.deletedev == true){
                axios.put("http://"+this.serverIP+":3000/api/rooms/"+this.roomID+"/deleteDevice/"+this.device._id, {},
                {headers: {
                    'auth-token': localStorage.getItem('auth-token')
                }}).then(response => {
                    console.log("Device id deleted: "+response.data)

                    // inserimento log
                    axios.post("http://"+this.serverIP+":3000/api/deviceLog/addLog/"+this.device._id,{
                        user: this.username,
                        //new_state: "same",
                        //new_state: this.device.status,
                        //quando viene disabilitato viene considerato spento
                        new_state: "false",
                        deviceModel: this.device.model,
                        moved_to: this.dummyID
                    },{headers: {
                        'auth-token': localStorage.getItem('auth-token')
                    }}).then(response => { console.log("Added log")})

                    this.eventBus.$emit("deviceUpdated", "")
                }).catch(error => {
                    console.log(error)
                })
            } else {
                axios.put("http://"+this.serverIP+":3000/api/rooms/"+this.roomID+"/moveDevice/"+this.device._id, {
                    newRoom: this.dummyID
                },
                {headers: {
                    'auth-token': localStorage.getItem('auth-token')
                }}).then(response => {
                    console.log("Device id deleted: "+response.data)
                    this.eventBus.$emit("deviceUpdated", "")
                }).catch(error => {
                    console.log(error)
                })
            }
        }
    },
    template: `
    <v-row justify="center">
    <v-dialog v-model="dialog" persistent max-width="600px">
      <template v-slot:activator="{ on, attrs }">
        <v-btn color="blue lighten-2" text v-bind="attrs" v-on="on">
            DISATTIVA/ELIMINA
        </v-btn>
      </template>
      <v-card>
        <v-card-title>
          <span class="headline">Disattiva/Elimina Dispositivo</span>
        </v-card-title>
        <v-card-text>
          <v-container>
          <v-form
          ref="form"
          lazy-validation
          >
          <v-radio-group v-model="deletedev" mandatory>
          <v-radio
          label="Disattiva il dispositivo"
          value="false"
        ></v-radio>
          <v-radio
            label="Elimina il dispositivo"
            value="true"
          ></v-radio>
        </v-radio-group>
            </v-form>
          </v-container>
        </v-card-text>
        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="blue darken-1" text @click="dialog = false">Chiudi</v-btn>
          <v-btn color="blue darken-1" text @click="deleteOrDisableDevice">Salva</v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
  </v-row>`
}

const LampCardItem = {
    props: ['device','roomID'],
    components: {
        'update-device': UpdateDeviceDialog,
        'delete-device': DeleteDeviceDialog
    },
    data: () => ({
        dummyID: null,
        switchLamp: false,
        interact: false,
    }),
    methods: {
        initDeviceCard(){
            axios.get("http://"+this.serverIP+":3000/api/rooms/dummy")
            .then(response => {
                this.dummyID = response.data[0]._id
            }).catch(error => {
                console.log(error)
            })

            this.switchLamp = this.device.state == "true" ? true : false
        },
        notifyRefreshDevice(){
            this.eventBus.$emit("deviceUpdated", "")
        }
    },
    computed: {
        isAdmin() {
            return this.$store.getters.isAdmin
        },
        username() {
            return this.$store.getters.getUsername
        },
        getLampState(){
            return this.switchLamp == true ? "Acceso" : "Spento"
        },
        eventBus() {
            return this.$store.getters.getEventBus
        }
      },
    created(){
        this.initDeviceCard()
    },
    watch: {
        switchLamp(value) {
            console.log("Lamp new state: "+value)
            axios.put("http://"+this.serverIP+":3000/api/rooms/"+this.roomID+"/updateDeviceState/"+this.device._id,
            {state: value},
            {headers: {
                'auth-token': localStorage.getItem('auth-token')
            }}).then(response => {
                console.log("Device id state updated: "+response.data)
                axios.post("http://"+this.serverIP+":3000/api/deviceLog/addLog/"+this.device._id,{
                    user: this.username,
                    new_state: value,
                    deviceModel: this.device.model,
                    moved_to: this.roomID
                },{headers: {
                    'auth-token': localStorage.getItem('auth-token')
                }}).then(response => { console.log("Added log switch lamp")})
                .catch(error => console.log(error))
            }).catch(error => {
                console.log(error)
            })
        }
    },
    template: `
    <div class="float-left">
        <v-card class="ma-5" min-width="220" max-width="300">
            <v-img height="150" :src="device.img" class="white--text align-end">
                <v-card-title class="pb-0">{{device.name}}</v-card-title>
            </v-img>
            <v-card-text class="ma-1 py-1">
            <div class="my-1 subtitle-1">{{device.model}}</div>
                <v-row align="center">
                    <v-col cols="6" sm="6" md="6">
                        <div>Stato: {{getLampState}}</div>
                        <div>Consumo: {{device.watt}} W/h</div>
                    </v-col>
                    <v-col cols="6" sm="6" md="6">
                        <v-btn v-if="interact == false" color="blue lighten-2" text @click="interact = true">
                            APRI
                        </v-btn>
                        <v-btn v-else color="blue lighten-2" text @click="interact = false">
                            CHIUDI
                        </v-btn>
                    </v-col>
                </v-row>
            </v-card-text>
            <div class="ma-1 pa-1" v-if="interact">
            <v-divider class="mx-4 my-1"></v-divider>
            <v-card-title>Comandi dispositivo</v-card-title>
            <v-card-text>
            <v-switch v-model="switchLamp" inset :label="getLampState"></v-switch>
            </v-card-text>
            <v-card-actions v-if="isAdmin == true">
                <update-device :device="device" :roomID="roomID" @deviceUpdated="notifyRefreshDevice"></update-device>
                <delete-device :device="device" :roomID="roomID" :dummyID="dummyID" @deviceUpdated="notifyRefreshDevice"></delete-device>
            </v-card-actions>
            </div>
        </v-card>
        </div>`
}

const ThermoCardItem = {
    props: ['device','roomID'],
    components: {
        'update-device': UpdateDeviceDialog,
        'delete-device': DeleteDeviceDialog
    },
    data: () => ({
        dummyID: null,
        actualTemp: null,
        tickLabels: [16, 16.5, 17, 17.5, 18, 18.5,
            19, 19.5, 20, 20.5, 21, 21.5, 22, 22.5,
            23, 23.5, 24, 24.5, 25, 25.5, 26, 26.5,
            27, 27.5, 28, 28.5, 29, 29.5, 30],
        interact: false,
    }),
    methods: {
        initDeviceCard(){
            axios.get("http://"+this.serverIP+":3000/api/rooms/dummy")
            .then(response => {
                this.dummyID = response.data[0]._id
            }).catch(error => {
                console.log(error)
            })

            this.actualTemp = parseInt(this.device.state)
        },
        notifyRefreshDevice(){
            this.eventBus.$emit("deviceUpdated", "")
        }
    },
    computed: {
        isAdmin() {
            return this.$store.getters.isAdmin
        },
        username() {
            return this.$store.getters.getUsername
        },
        getTempState(){
            return this.actualTemp
        },
        eventBus() {
            return this.$store.getters.getEventBus
        }
      },
    created(){
        this.initDeviceCard()
    },
    watch: {
        actualTemp(value){
            console.log("Temp new state: "+value)
            axios.put("http://"+this.serverIP+":3000/api/rooms/"+this.roomID+"/updateDeviceState/"+this.device._id,
            {state: value.toString()},
            {headers: {
                'auth-token': localStorage.getItem('auth-token')
            }}).then(response => {
                console.log("Device id state updated: "+response.data)
                // inserimento log
                axios.post("http://"+this.serverIP+":3000/api/deviceLog/addLog/"+this.device._id,{
                    user: this.username,
                    new_state: value.toString(),
                    deviceModel: this.device.model,
                    moved_to: this.roomID
                },{headers: {
                    'auth-token': localStorage.getItem('auth-token')
                }}).then(response => { console.log("Added log")})
            }).catch(error => {
                console.log(error)
            })
        }
    },
    template: `
    <div class="float-left">
        <v-card class="ma-5" min-width="220" max-width="300">
            <v-img height="150" :src="device.img" class="white--text align-end">
                <v-card-title class="pb-0">{{device.name}}</v-card-title>
            </v-img>
            <v-card-text class="ma-1 py-1">
            <div class="my-1 subtitle-1">{{device.model}}</div>
                <v-row align="center">
                    <v-col cols="6" sm="6" md="6">
                        <div>Stato: {{getTempState}} °C</div>
                        <div>Consumo: {{device.watt}} W/h</div>
                    </v-col>
                    <v-col cols="6" sm="6" md="6">
                        <v-btn v-if="interact == false" color="blue lighten-2" text @click="interact = true">
                            APRI
                        </v-btn>
                        <v-btn v-else color="blue lighten-2" text @click="interact = false">
                            CHIUDI
                        </v-btn>
                    </v-col>
                </v-row>
            </v-card-text>
            <div class="ma-1 pa-1" v-if="interact">
            <v-divider class="mx-4 my-1"></v-divider>
            <v-card-title>Comandi dispositivo</v-card-title>
            <v-card-text>
            <div>
                <v-col class="text-center">
                    <span class="display-3 font-weight-light" v-text="actualTemp"></span>
                    <span class="subheading font-weight-light mr-1"> °C</span>
                </v-col>
                <v-slider
                v-model="actualTemp"
                color="blue lighten-2"
                track-color="grey"
                always-dirty
                min="16"
                max="30"
                >
                    <template v-slot:prepend>
                    <v-icon color="blue lighten-2" @click="actualTemp--">
                        mdi-minus
                    </v-icon>
                    </template>

                    <template v-slot:append>
                    <v-icon color="blue lighten-2" @click="actualTemp++">
                        mdi-plus
                    </v-icon>
                    </template>
                </v-slider>
            </div>
            </v-card-text>
            <v-card-actions v-if="isAdmin == true">
                <update-device :device="device" :roomID="roomID" @deviceUpdated="notifyRefreshDevice"></update-device>
                <delete-device :device="device" :roomID="roomID" :dummyID="dummyID" @deviceUpdated="notifyRefreshDevice"></delete-device>
            </v-card-actions>
            </div>
        </v-card>
        </div>`
}

const CardItem = {
    props: ['device','roomID'],
    components: {
        'lamp-card': LampCardItem,
        'thermo-card': ThermoCardItem
    },
    template: `
    <div v-if="device.category == 'lamp'">
        <lamp-card :device="device" :roomID="roomID"></lamp-card>
    </div>
    <div v-else-if="device.category == 'temp'">
        <thermo-card :device="device" :roomID="roomID"></thermo-card>
    </div>
    `
}

const AddDeviceDialog = {
    props: ['roomID'],
    data: () => ({
        models: [],
        dummyID: null,
        selectDevice: null,
        devicesDisabled: [],
        devicesDisabledNames: [],
        chosenModel: null,
        modelRules: [v => !!v || 'Model is required'],
        dialog: false,
        name: "",
        nameRules: [v => !!v || 'Name is required'],
        img: "",
        imgRules: [v => !!v || 'URL is required']
    }),
    computed: {
        eventBus() {
          return this.$store.getters.getEventBus
        }
    },
    methods: {
        addNewDevice(){
            this.dialog = false
            if(this.selectDevice == null){
                axios.get("http://"+this.serverIP+":3000/api/deviceType/"+this.chosenModel.split(" ")[0],
                {headers: {
                    'auth-token': localStorage.getItem('auth-token')
                }}).then(responseType => {
                        axios.put("http://"+this.serverIP+":3000/api/rooms/"+this.roomID+"/addDevice", {
                            name: this.name,
                            img: this.img,
                            model: this.chosenModel.split(" ")[0],
                            category: responseType.data[0].category,
                            watt: responseType.data[0].watt
                        },{headers: {
                            'auth-token': localStorage.getItem('auth-token')
                        }}).then(responseAdded => {
                            this.eventBus.$emit("deviceUpdated", "")
                        })
                    }).catch(error => {
                        console.log(error)
                    })
            } else {
                var deviceID
                this.devicesDisabled.forEach(dev => {
                    if(dev.name == this.selectDevice)
                        deviceID = dev._id
                })
                axios.put("http://"+this.serverIP+":3000/api/rooms/"+this.dummyID+"/moveDevice/"+deviceID, {
                    newRoom: this.roomID
                },{headers: {
                    'auth-token': localStorage.getItem('auth-token')
                }}).then(response => {
                    this.eventBus.$emit("deviceUpdated", "")
                }).catch(error => {
                    console.log(error)
                })
            }
        },
        setModels(){
            axios.get("http://"+this.serverIP+":3000/api/deviceType")
            .then(response => {
                this.models = []
                response.data.forEach(element => {
                    this.models.push(element.model+" - "+element.name+" "+element.watt+"W")
                })
            }).catch(error => {
                console.log(error)
            })
        },
        initDisabledDevices(){
            axios.get("http://"+this.serverIP+":3000/api/rooms/dummy")
            .then(response => {
                this.dummyID = response.data[0]._id
                response.data[0].devices.forEach(dev => {
                    this.devicesDisabled.push(dev)
                    this.devicesDisabledNames.push(dev.name)
                })
            }).catch(error => {
                console.log(error)
            })
        }
    },
    mounted(){
        this.setModels()
        this.initDisabledDevices()
    },
    template: `
    <v-row justify="center">
    <v-dialog v-model="dialog" persistent max-width="600px">
        <template v-slot:activator="{ on, attrs }">
        <v-btn fab dark large bottom right fixed color="pink" v-bind="attrs" v-on="on">
            <v-icon>mdi-plus</v-icon>
        </v-btn>
        </template>
        <v-card>
        <v-card-title>
            <span class="headline">Nuovo Dispositivo</span>
        </v-card-title>
        <v-card-text>
            <v-container>
            <v-form
            ref="form"
            lazy-validation
            >
            <v-row>
                <v-col cols="6" sm="6" md="6">
                <v-text-field
                label="Nome*"
                required
                hint="Inserisci il nome del nuovo Dispositivo"
                persistent-hint
                :rules="nameRules"
                v-model="name"
                ></v-text-field>
                </v-col>
                <v-col class="d-flex" cols="6" sm="6">
                <v-select
                  v-model="chosenModel"
                  :rules="modelRules"
                  hint="Scegli il modello del nuovo Dispositivo"
                  :items="models"
                  label="Modello"
                ></v-select>
                </v-col>
            </v-row>
            <v-row>
                <v-col cols="12" sm="12" md="12">
                <v-text-field
                label="URL Immagine*"
                required
                hint="Inserisci l'URL dell'immagine per il tuo nuovo Dispositivo"
                persistent-hint
                :rules="imgRules"
                v-model="img"
                ></v-text-field>
                </v-col>
            </v-row>
            </v-form>
            </v-container>
            <small>oppure scegli il dispositivo tra quelli disabilitati</small>
            <v-container>
                <v-form
                ref="form"
                lazy-validation>
                    <v-select
                    v-model="selectDevice"
                    hint="Scegli il dispositivo da aggiungere a questa stanza"
                    :items="devicesDisabledNames"
                    label="Dispositivi disabilitati"
                    ></v-select>
                </v-form>
            </v-container>
        </v-card-text>
        <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn color="blue darken-1" text @click="dialog = false">Close</v-btn>
            <v-btn color="blue darken-1" text @click="addNewDevice">Save</v-btn>
        </v-card-actions>
        </v-card>
    </v-dialog>
    </v-row>`
}

const Devices = {
    data: () => ({
        devices: [],
        room: ""
    }),
    components: {
        'card-item': CardItem,
        'new-device': AddDeviceDialog
    },
    methods: {
        initDevices(){
            var roomName = this.$route.params.room
            axios.get("http://"+this.serverIP+":3000/api/rooms/"+roomName)
            .then(response => {
                this.room = response.data[0]
                this.devices = response.data[0].devices
                this.eventBus.$emit('updateBreadcrumb', [
                    {
                        text: 'Home',
                        disabled: false,
                        href: '/home'
                    },{text: '/'},{
                        text: 'Stanze',
                        disabled: false,
                        href: '/roomsdevices'
                    },{text: '/'},{
                        //text: 'Dispositivi '+this.room.name,
                        text: this.room.name,
                        disabled: false,
                        href: '/devices/'+this.room.name
                    }])
            }).catch(error => (this.$refs.messagebar.error(error.response.data)))
        }
    },
    computed: {
        isAdmin() {
            return this.$store.getters.isAdmin;
        },
        username() {
            return this.$store.getters.getUsername;
        },
        eventBus() {
          return this.$store.getters.getEventBus
        }
      },
      mounted(){
          this.eventBus.$on("deviceUpdated", data => {
            this.initDevices()
          })
          this.initDevices()
      },
    template:`
    <v-container fluid>
      <v-layout wrap>
        <div v-for="device in devices" :key=device._id>
          <v-flex d-flex>
                <card-item :device="device" :roomID="room._id" @deleteDevice="initDevices" :fullscreen="$vuetify.breakpoint.mobile"></card-item>
          </v-flex>
        </div>
            <new-device v-if="isAdmin == true" :roomID="room._id" @addDevice="initDevices"></new-device>
      </v-layout>
    </v-container>`
}
