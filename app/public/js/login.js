//TODO: creare un componente dedicato alla snackbar?
const Login = {
	template: `
	<div>
		<v-snackbar
			v-model="snackbar"
			:timeout="4000"
			top
			:color="snackbarColor"
		>
			<span> {{snackbarMessage}} </span>
			<template v-slot:action="{ attrs }">
		        <v-btn
			        dark
			        text
			        v-bind="attrs"
			        @click="snackbar = false"
		        >
		        	Close
		        </v-btn>
	      </template>

		</v-snackbar>

			<v-container
	          class="fill-height"
	          fluid
	        >
		        <v-row
		        	align="center"
					justify="center"
		        >
		            <v-col
		              cols="12"
		              sm="8"
		              md="4"
		            >
						<v-card class="mx-auto mt-5 elevation-12" align="center" justify="center">
							<v-toolbar
							   color="primary"
							   dark
							   flat
							 >
						   	<v-toolbar-title>Login</v-toolbar-title>
							</v-toolbar>
							<v-card-text>
								<v-form
								ref="form"
								lazy-validation
								>
									<v-text-field
										label="Username"
										prepend-icon="mdi-account-circle"
										v-model="username"
										:rules="usernameRules"
										required
									/>
									<v-text-field
										:type="showPassword ? 'text': 'password' "
										label="Password"
										prepend-icon="mdi-lock"
										:append-icon="showPassword ? 'mdi-eye' : 'mdi-eye-off'"
										@click:append="showPassword = !showPassword"
										v-model="password"
										:rules="passwordRules"
										required
									/>
								</v-form>
							</v-card-text>
							<v-divider></v-divider>
							<v-card-actions class="justify-center">
								<v-btn color="success" @click="handleSubmit">Login</v-btn>
							</v-card-actions>
						</v-card>
					</v-col>
	 			</v-row>
   			</v-container>
		</div>
	`,
	data() {
		return {
			username: "",
			usernameRules: [
		        v => !!v || 'Name is required',
		    ],
			password: "",
			passwordRules: [
				v => !!v || 'Password is required',
				v => (v && v.length >= 8) || 'Password must be less than 8 characters',
			],
			showPassword: false,
			snackbar: false,
			snackbarMessage: '',
			snackbarColor: ''
		}
	},
	methods: {
		resetValidation() {
			this.$refs.form.resetValidation()
		},
		handleSubmit() {
			console.log("submit")
			console.log(this.username)
			console.log(this.password)
			if (this.$refs.form.validate()) {
				axios.post("http://"+this.serverIP+":3000/api/user/login", {
					username: this.username,
					password: this.password,
				}).then(response => {
					this.snackbarMessage = "Login effettuato"
					this.snackbarColor = "success"
					this.snackbar = true
					//dice a vuex che è stato fatto il login
					//committa il verificarsi di questo evento sullo store
					localStorage.setItem('auth-token', response.data)
					localStorage.setItem('username', this.username)
					this.$store.commit('login')
					console.log(response.data)
					//this.$root.$emit('ciao', this.username)
					this.$router.push({ path: '/home', name: 'Home' })
				}).catch(error => {
					(console.log(error))
					this.snackbarMessage = error
					this.snackbarColor = "error"
					this.snackbar = true
				});
			}
		},
		init() {
			this.resetValidation()
		}
	},
	mounted(){
		this.init()
	}
}
