const ModifyPINDialog = {
    props: ['item'],
    data: () => ({
        dialog: false,
        oldPIN: null,
        newPIN: null,
        showPassword: false,
        passwordRules: [
            v => !!v || 'PIN is required',
            v => (v && v.length == 4) || 'PIN must be 4 characters',
        ],
    }),
    template: `
    <v-row justify="center">
    <v-dialog v-model="dialog" persistent max-width="600px">
      <template v-slot:activator="{ on, attrs }">
        <v-btn fab dark large bottom right fixed color="pink" v-bind="attrs" v-on="on">
            <v-icon>mdi-cog-outline</v-icon>
        </v-btn>      
      </template>
      <v-card>
        <v-card-title>
          <span class="headline">Modifica PIN</span>
        </v-card-title>
        <v-card-text>
        <v-container>
        <v-form>
            <v-text-field 
            :type="showPassword ? 'text': 'password' "
            label="Vecchio PIN"
            prepend-icon="mdi-lock"
            :append-icon="showPassword ? 'mdi-eye' : 'mdi-eye-off'"
            @click:append="showPassword = !showPassword"
            required
            hint="Inserisci il vecchio PIN"
            persistent-hint
            :rules="passwordRules"
            v-model="oldPIN"
            ></v-text-field>
            <v-text-field
            :type="showPassword ? 'text': 'password' "
            label="Nuovo PIN"
            prepend-icon="mdi-lock"
            :append-icon="showPassword ? 'mdi-eye' : 'mdi-eye-off'"
            @click:append="showPassword = !showPassword"
            required
            hint="Inserisci il nuovo PIN"
            persistent-hint
            :rules="passwordRules"
            v-model="newPIN"
            ></v-text-field>
      </v-form>
      </v-container>
        </v-card-text>
        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="blue darken-1" text @click="dialog = false">CHIUDI</v-btn>
          <v-btn color="blue darken-1" text @click="modifyPIN">AVANTI</v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
    </v-row>`,
    methods: {
        modifyPIN(){
            axios.post("http://"+this.serverIP+":3000/api/alarm/verify",{
                pin: this.oldPIN
                },{headers: {
                    'auth-token': localStorage.getItem('auth-token')
                }}).then(res => {
                  if(res.status == 200){
                    axios.put("http://"+this.serverIP+":3000/api/alarm/update",{
                        pin: this.newPIN
                    },{headers: {
                        'auth-token': localStorage.getItem('auth-token')
                      }}).then(response => {
                        console.log("new pin: "+this.newPIN)
                        this.dialog = false
                    }).catch(err => console.log(err))
                  } else 
                    console.log("wrong pin")
                }).catch(error => console.log(error))
        }
    }
}

const HistoryCard = {
    template: `
        <v-card class="ma-5" min-width="220" max-width="300" :fullscreen="$vuetify.breakpoint.mobile">
            <v-img class="white--text align-end" height="100px" src="https://unsplash.com/photos/f5xddISq428/download?force=true&w=1920">
                <v-card-title class="pb-0">Storico Allarme</v-card-title>
            </v-img>
            <v-card-subtitle class="pb-0">Visualizza tutte le attivazioni dell'allarme</v-card-subtitle>
            <v-card-actions>
                <v-btn color="blue lighten-2" @click="openAlarmHistory" text> APRI </v-btn>
            </v-card-actions>
        </v-card>`,
        methods: {
            openAlarmHistory(){
                this.$router.push({ path: '/alarm/history', name: 'AlarmHistory' })
            }
        }
}

const DeactivateAlarmDialog = {
    data: () => ({
        dialog: false,
        rooms: [],
        showPassword: false,
        password: "",
        passwordRules: [
            v => !!v || 'PIN is required',
            v => (v && v.length == 4) || 'PIN must be 4 characters',
        ],
    }),
    template: `
    <v-row justify="center">
    <v-dialog v-model="dialog" scrollable>
    <template v-slot:activator="{ on, attrs }">
        <div v-bind="attrs" :v-on="on"></div>
    </template>
        <v-card min-width="220" :fullscreen="$vuetify.breakpoint.mobile">
            <v-img class="white--text align-end" height="100px" src="https://unsplash.com/photos/wXs0mncHyfI/download?force=true&w=1920">
                <v-card-title class="pb-0">Disattiva Allarme</v-card-title>
            </v-img>
            <div class="ma-1 pa-1" v-if="this.dialog == true">
                <v-card-title>Disattiva Allarme</v-card-title>
                <v-card-subtitle class="pb-0">Inserisci il PIN per disattivare l'allarme:</v-card-subtitle>
                <v-card-text>
                    <v-form ref="form" lazy-validation>
                        <v-text-field
                            :type="showPassword ? 'text': 'password' "
                            label="PIN"
                            prepend-icon="mdi-lock"
                            :append-icon="showPassword ? 'mdi-eye' : 'mdi-eye-off'"
                            @click:append="showPassword = !showPassword"
                            v-model="password"
                            :rules="passwordRules"
                            required
                        />
                    </v-form>
                </v-card-text>
            </div>
            <v-card-actions>
            <v-btn color="blue lighten-2" @click="deactivateAlarm" text> DISATTIVA </v-btn>
            </v-card-actions>
        </v-card>
        </v-dialog>
        </v-row>`,
        computed: {
            isActive(){
                return this.dialog == true
            },
            alarmState() {
                return this.dialog == true ? "Acceso" : "Spento"
            },
            eventBus() {
                return this.$store.getters.getEventBus
            }
        },
        methods: {
            deactivateAlarm(){
                if(this.dialog == true){
                    axios.post("http://"+this.serverIP+":3000/api/alarm/verify",{
                        pin: this.password
                        },{headers: {
                            'auth-token': localStorage.getItem('auth-token')
                        }}).then(res => {
                        if(res.status == 200){
                            this.dialog = false
                            while(this.rooms.lenght != 0){
                                var item = this.rooms.pop()
                                axios.put("http://"+this.serverIP+":3000/api/alarm/deactivate/"+item._id,{},
                                {headers: {
                                    'auth-token': localStorage.getItem('auth-token')
                                }}).then(response => {
                                    console.log("Alarm deactivated")
                                    this.eventBus.$emit("alarmInactive", "")
                                }).catch(err => console.log(err))
                            }
                        } else 
                            console.log("wrong pin")
                        }).catch(error => console.log(error))
                    console.log("pin: "+this.password)
                }
            }
        },
    mounted() {
        this.eventBus.$on("alarmActive", data => {
            this.dialog = true
            this.rooms.push(data)
        })
    }
}

const AlarmRoomCard = {
    props: ['item'],
    data: () => ({
        active: false,
    }),
    computed: {
        isActive(){
            return this.active == true
        },
        alarmState() {
            return this.active == true ? "Acceso" : "Spento"
        },
        eventBus() {
            return this.$store.getters.getEventBus
        }
    },
    template: `
        <v-card class="ma-5" min-width="220" max-width="300" :fullscreen="$vuetify.breakpoint.mobile">
            <v-img class="white--text align-end" height="100px" v-bind:src=item.img>
                <v-card-title class="pb-0">{{item.name}}</v-card-title>
            </v-img>
            <v-card-subtitle class="pb-0">Stato Allarme: {{ alarmState }}</v-card-subtitle>
            <v-card-actions>
                <v-btn color="blue lighten-2" v-if="this.active == false" @click="activateAlarm" text> ATTIVA </v-btn>
            </v-card-actions>
        </v-card>`,
        methods: {
            activateAlarm(){
                axios.put("http://"+this.serverIP+":3000/api/alarm/activate/"+this.item._id,{})
                .then(response => {
                    this.active = true
                    console.log("Alarm activated")
                    this.eventBus.$emit("alarmActive", this.item)
                }).catch(error => console.log(error))
            }
        },
    mounted() {
        this.active = this.item.alarm_state
        if(this.active == true) this.eventBus.$emit("alarmActive", this.item)

        this.eventBus.$on("alarmInactive", data => {
            this.active = false
        })
    }
}

const Alarm = {
    components: {
        'message-bar': MessageBar,
        'pin-dialog': ModifyPINDialog,
        'alarm-room': AlarmRoomCard,
        'alarm-history': HistoryCard,
        //'deactivate-alarm': DeactivateAlarmCard
        'deactivate-dialog': DeactivateAlarmDialog
    },
    computed: {
      isAdmin() {
      	return this.$store.getters.isAdmin;
      },
      username() {
      	return this.$store.getters.getUsername;
      },
      eventBus() {
        return this.$store.getters.getEventBus
      }
    },
    template: `
    <div>
      <v-container fluid>
      <deactivate-dialog></deactivate-dialog>
        <v-layout wrap>
          <div v-for="item in items" :key=item._id>
            <v-flex d-flex>
                <div v-if="item == 'history'" class="float-left">
                    <alarm-history></alarm-history>
                </div>
                <div v-else class="float-left">
                    <alarm-room :item="item"></alarm-room>
                </div>
            </v-flex>
          </div>
          <pin-dialog v-if="isAdmin == true"></pin-dialog>
        </v-layout>
      </v-container>
    </div>`,
    data: () => ({
        items: []
    }),
    methods: {
        initRooms(){
          axios.get("http://"+this.serverIP+":3000/api/rooms")
            .then(response => {
                this.items = []
                this.items.push("history")
                //this.items.push("deactivate")
                response.data.forEach(room => {
                    this.items.push(room)
                })
                console.log(this.items)
            }).catch(error => (this.$refs.messagebar.error(error.response.data)))
        }
    },
    mounted(){
        this.eventBus.$emit('updateBreadcrumb', [
            {
                text: 'Home',
                disabled: false,
                href: '/home'
            },{text: '/'},{
                text: 'Allarme',
                disabled: false,
                href: '/alarm'
            }])
        this.initRooms()
    }
}