const Notify = {
  template: `
  <div>
  <v-card>
      <v-toolbar dark color="primary">
        <v-btn icon dark @click="dialog = false">
          <v-icon>mdi-close</v-icon>
        </v-btn>
        <v-toolbar-title>Settings</v-toolbar-title>
        <v-spacer></v-spacer>
        <v-toolbar-items>
          <v-btn dark text @click="dialog = false">Save</v-btn>
        </v-toolbar-items>
      </v-toolbar>
      <v-list three-line subheader>
        <v-subheader>User Controls</v-subheader>
        <v-list-item>
          <v-list-item-content>
            <v-list-item-title>Content filtering</v-list-item-title>
            <v-list-item-subtitle>Set the content filtering level to restrict apps that can be downloaded</v-list-item-subtitle>
          </v-list-item-content>
        </v-list-item>
        <v-list-item>
          <v-list-item-content>
            <v-list-item-title>Password</v-list-item-title>
            <v-list-item-subtitle>Require password for purchase or use password to restrict purchase</v-list-item-subtitle>
          </v-list-item-content>
        </v-list-item>
      </v-list>
      <v-divider></v-divider>
      <v-list three-line subheader>
        <v-subheader>General</v-subheader>
        <v-list-item>
          <v-list-item-action>
            <v-checkbox v-model="notifications"></v-checkbox>
          </v-list-item-action>
          <v-list-item-content>
            <v-list-item-title>Notifications</v-list-item-title>
            <v-list-item-subtitle>Notify me about updates to apps or games that I downloaded</v-list-item-subtitle>
          </v-list-item-content>
        </v-list-item>
        <v-list-item>
          <v-list-item-action>
            <v-checkbox v-model="sound"></v-checkbox>
          </v-list-item-action>
          <v-list-item-content>
            <v-list-item-title>Sound</v-list-item-title>
            <v-list-item-subtitle>Auto-update apps at any time. Data charges may apply</v-list-item-subtitle>
          </v-list-item-content>
        </v-list-item>
        <v-list-item>
          <v-list-item-action>
            <v-checkbox v-model="widgets"></v-checkbox>
          </v-list-item-action>
          <v-list-item-content>
            <v-list-item-title>Auto-add widgets</v-list-item-title>
            <v-list-item-subtitle>Automatically add home screen widgets</v-list-item-subtitle>
          </v-list-item-content>
        </v-list-item>
      </v-list>
    </v-card>
    </div>
  `,
  data () {
    return {
      dialog: false,
      notifications: false,
      sound: true,
      widgets: false,
    }
  }
}

const Breadcrumbs = {
  data: ()  => ({
    breadcrumbs:[]
  }),
  computed:{
    eventBus() {
      return this.$store.getters.getEventBus
    },
    items(){
      return this.breadcrumbs
    }
  },
  template: `
    <v-breadcrumbs divider="/" dark>
      <v-breadcrumbs-item v-for="item in items" :key="item.href" :href="item.href">
        <p id="breadcrumb" class="font-weight-medium">{{item.text}}</p>
      </v-breadcrumbs-item>
    </v-breadcrumbs>
  `,
  mounted(){
    this.eventBus.$on('updateBreadcrumb', data => {
      this.breadcrumbs = data
    })
  }
}

const Navbar = {
  components: {
    "breadcrumbs": Breadcrumbs
  },
    template: `
    <div>
        <v-navigation-drawer v-model="drawer" :clipped="$vuetify.breakpoint.lgAndUp" app>
          <v-list dense v-if="logged">
            <div v-for="item in items" :key="items._id">
              <v-row v-if="item.heading" :key="item.heading" align="center">
                <v-col cols="6">
                  <v-subheader v-if="item.heading">
                    {{ item.heading }}
                  </v-subheader>
                </v-col>
              </v-row>
              <v-list-item v-else :key="item.name" link>
                <v-list-item-action>
                  <v-icon>{{ item.icon }}</v-icon>
                </v-list-item-action>
                <v-list-item-content>
                  <router-link class="nav-link" v-bind:to="item.link" color="black">{{ item.name }}</router-link>
                </v-list-item-content>
              </v-list-item>
            </div>
          </v-list>
          <v-list dense v-else>
            <v-list-item link>
              <v-list-item-action>
                <v-icon>mdi-check-circle</v-icon>
              </v-list-item-action>
              <v-list-item-content>
                <router-link class="nav-link" to="/login" color="black">LOGIN</router-link>
              </v-list-item-content>
            </v-list-item>
            <v-list-item link>
              <v-list-item-action>
                <v-icon>mdi-account-plus</v-icon>
              </v-list-item-action>
              <v-list-item-content>
                <router-link class="nav-link" to="/signup" color="black">REGISTRATI</router-link>
              </v-list-item-content>
            </v-list-item>
          </v-list>

          <template v-slot:append v-if="logged">
            <div class="pa-2">
              <v-btn block dark @click.prevent="signout">Logout</v-btn>
            </div>
          </template>

        </v-navigation-drawer>
        <v-app-bar :clipped-left="$vuetify.breakpoint.lgAndUp" app color="primary" dark>
          <v-app-bar-nav-icon @click.stop="drawer = !drawer"></v-app-bar-nav-icon>
          <v-toolbar-title class="ml-0 pl-4">
            <breadcrumbs></breadcrumbs>
          </v-toolbar-title>
          <v-spacer></v-spacer>
            <v-btn icon v-if="logged" @click="loadNotificationPage">
              <v-badge v-if="this.notifications.length > 0" color="red" v-bind:content="this.notifications.length" overlap left>
                <v-icon>mdi-bell</v-icon>
              </v-badge>
            </v-btn>
        </v-app-bar>
    </div>
    `,
    data(){
      return {
        dialog: false,
        drawer: false,
        items: [],
        notifications: []
      }
    }, computed: {
      logged() {
        console.log("computed accessed", this.$store.getters.isLogged)
        return this.$store.getters.isLogged;
      },
      username() {
        return this.$store.getters.getUsername
      },
      eventBus() {
        return this.$store.getters.getEventBus
      }
    },
    methods: {
        setUserMenu(){
          this.items = []
          axios.get("http://"+this.serverIP+":3000/api/features/user")
                .then(response => {
                    this.items = response.data
                }).catch(error => (this.$refs.messagebar.error(error.response.data)))
        },
        checkIfIsLogged(){
          console.log("CHECK LOGGED")
          return 'auth-token' in localStorage
        },
        signout(){
          console.log("signout")
          this.$store.commit("logout")
          localStorage.removeItem('auth-token')
          this.$router.push('/')
        },
        getUserNotifications(){
          console.log(`http://${this.serverIP}:3000/api/notifications/users/${this.username}`)
          console.log(localStorage.getItem('auth-token'))
          axios
            axios.get(`http://${this.serverIP}:3000/api/notifications/users/${this.username}`, {headers: {
                'auth-token': localStorage.getItem('auth-token')
            }}).then(response => this.notifications = response.data)
        },
        loadNotificationPage() {
            router.push(`/notifications`)
        }
    },
    mounted(){
      //this.$root.$on('geo', data => console.log(data))
      this.setUserMenu()
      this.getUserNotifications()
      this.eventBus.$on("ReloadNotifications", msg => this.getUserNotifications())

    }
}
