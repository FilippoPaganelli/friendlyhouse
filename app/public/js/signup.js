const Signup = {
	components: {
		'message-bar': MessageBar
	},
	template: `
	<div>
		<message-bar ref="messageBar"></message-bar>
			<v-container
	          class="fill-height"
	          fluid
            >
            
		        <v-row
		        	align="center"
					justify="center"
		        >
		            <v-col
		              cols="12"
		              sm="8"
		              md="4"
		            >
						<v-card class="mx-auto mt-5 elevation-12" align="center" justify="center">
							<v-toolbar
							   color="primary"
							   dark
							   flat
							 >
						   	<v-toolbar-title>Registrazione</v-toolbar-title>
							</v-toolbar>
							<v-card-text>
								<v-form
								ref="form"
								lazy-validation
								>
									<v-text-field
										label="Username"
										prepend-icon="mdi-account-circle"
										v-model="username"
										:rules="usernameRules"
										required
									/>
									<v-text-field
										:type="showPassword ? 'text': 'password' "
										label="Password"
										prepend-icon="mdi-lock"
										:append-icon="showPassword ? 'mdi-eye' : 'mdi-eye-off'"
										@click:append="showPassword = !showPassword"
										v-model="password"
										:rules="passwordRules"
										required
									/>
								</v-form>
							</v-card-text>
							<v-divider></v-divider>
							<v-card-actions>
								<v-btn color="success" @click="handleSubmit" >Registrati</v-btn>
							</v-card-actions>
						</v-card>
					</v-col>
	 			</v-row>
   			</v-container>
	</div>
	`,
	data() {
		return {
			username: "",
			usernameRules: [
		        v => !!v || 'Name is required',
		    ],
			password: "",
			passwordRules: [
				v => !!v || 'Password is required',
				v => (v && v.length >= 8) || 'Password must be less than 8 characters',
			],
			role: 'user',
			showPassword: false,
			snackbar: false,
			snackbarMessage: '',
			snackbarColor: ''
		}
	},
	methods: {
		resetValidation() {
			this.$refs.form.resetValidation()
		},
		handleSubmit() {
			if (this.$refs.form.validate()) {
				console.log("isValid")
				//make a submit

				axios.post("http://"+this.serverIP+":3000/api/user/register", {
					username: this.username,
					password: this.password,
					role: this.role
				}).then(response => {
					console.log(response)
					console.log("ok")
					this.$refs.messageBar.success("Utente registrato!")
					
					// Login after signup
					axios.post("http://"+this.serverIP+":3000/api/user/login", {
						username: this.username,
						password: this.password,
					}).then(response => {
						console.log(response.data)
						localStorage.setItem('auth-token', response.data)
						localStorage.setItem('username', this.username)
						//this.$refs.navbar.checkIfIsLogged()
						this.$router.push({ path: '/home', name: 'Home' })
					}).catch(error => {
						console.log(error)
					});

				}).catch(error => {
					(console.log(error.response.data))
					this.$refs.messageBar.error(error.response.data)
				});

			} else {
				console.log("is not valid")
			}
		},
		init() {
			this.resetValidation()
		}
	},
	mounted(){
		this.init()
	}
}