const Entry = {
    components: {
        'login': Login,
        'signup': Signup
    },
    data: () => ({
        
    }),
    methods: {
        checkIfISLogged(){
          return 'auth-token' in localStorage
        }
    },
    mounted(){
        if(this.checkIfISLogged())
          this.$router.push({ path: '/home', name: 'Home' })
    },
    template: `
  <v-parallax
    dark
    src="https://images.pexels.com/photos/186077/pexels-photo-186077.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260"
  >
    <v-row
      align="center"
      justify="center"
    >
      <v-col class="text-center" cols="12">
        <h1 class="display-1 font-weight-bold mb-4">FriendlyHouse</h1>
        <h4 class="subheading">Build your application today!</h4>
      </v-col>
    </v-row>
  </v-parallax>
    `
}