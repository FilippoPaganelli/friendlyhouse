const GPS = {
    data: () => ({
        threshold: 0.0020,
        step: 0.0020,
        pos: {
            latitude: null,
            longitude: null
        },
        far: false
    }),
    computed: {
        username() {
            return this.$store.getters.getUsername
        },
        eventBus() {
            return this.$store.getters.getEventBus
        }
    },
    methods: {
        getSimulatedLocation(){
            var home_position = this.checkHomeNearness()
            console.log(home_position)

            if(home_position == "far"){
                // utente si avvicina alla abitazione
                this.pos.latitude = (this.pos.latitude - this.step)
                this.pos.longitude = (this.pos.longitude - this.step)
                /*console.log("lat: "+this.pos.latitude)
                console.log("lon: "+this.pos.longitude)
                console.log("home-lat: "+this.homePos.latitude)
                console.log("home-lon: "+this.homePos.longitude)*/
            } else if (home_position == "near"){
                // utente si allontana dalla abitazione
                this.pos.latitude = (this.pos.latitude + this.step)
                this.pos.longitude = (this.pos.longitude + this.step)
                /*console.log("lat: "+this.pos.latitude)
                console.log("lon: "+this.pos.longitude)
                console.log("home-lat: "+this.homePos.latitude)
                console.log("home-lon: "+this.homePos.longitude)*/
            }

            // emit geo event: latitude and longitude
            this.eventBus.$emit('geo', this.pos)
            if((this.far == true && home_position == "near") || (this.far == false && home_position == "far")){
                this.far = !this.far
                axios.put("http://"+this.serverIP+":3000/api/gps/"+this.username, {
                    home_pos: home_position,
                    latitude: this.pos.latitude,
                    longitude: this.pos.longitude
                },{headers: {
                    'auth-token': localStorage.getItem('auth-token')
                }}).then(response => {
                    console.log("Added new position for user "+this.username)
                }).catch(error => {
                    console.log(error)
                })
            }

        },
        getLocation() {
            if(this.username != ""){
                /*navigator.geolocation.getCurrentPosition(position => {
                    this.pos.latitude = position.coords.latitude
                    this.pos.longitude = position.coords.longitude
                    console.log("Latitude: "+this.latitude)
                    console.log("Longitude: "+this.longitude)
                })*/

                this.pos = {
                    latitude: geoplugin_latitude(),
                    longitude: geoplugin_longitude()
                }

                if(this.pos.latitude == null || this.pos.longitude == null)
                    console.log("Error: cannot get user location")
                else {
                    // emit geo event: latitude and longitude
                    this.eventBus.$emit('geo', this.pos)
                    var home_position = this.checkHomeNearness()
                    if((this.far == true && home_position == "near") || (this.far == false && home_position == "far")){
                        this.far = !this.far
                        axios.put("http://"+this.serverIP+":3000/api/gps/:user"+this.username, {
                            home_pos: home_position,
                            latitude: this.pos.latitude,
                            longitude: this.pos.longitude
                        },{headers: {
                            'auth-token': localStorage.getItem('auth-token')
                        }}).then(response => {
                            console.log("Added new position for user "+this.username)
                        }).catch(error => {
                            console.log(error)
                        })
                    }
                }
            }
        },
        checkHomeNearness(){
            var absoluteLatitudeDelta = Math.abs(this.pos.latitude - this.homePos.latitude)
            var absoluteLongitudeDelta = Math.abs(this.pos.longitude - this.homePos.longitude)
            var deltaMax = this.threshold*2
            if(absoluteLatitudeDelta > deltaMax && absoluteLongitudeDelta > deltaMax){
                // emit event to signal that the user is walking away from the home
                this.eventBus.$emit('far', this.pos)
                return "far"
            } else if(absoluteLatitudeDelta <= deltaMax && absoluteLongitudeDelta <= deltaMax){
                // emit event to signal that the user is walking closer to the home
                this.eventBus.$emit('near', this.pos)
                return "near"
            } else {
                // in casa
                return "near"
            }
        }
    },
    created(){
        // SIMULATED GPS
        /*this.pos.latitude = this.homePos.latitude
        this.pos.longitude = this.homePos.longitude
        this.far = false
        this.eventBus.$on('geoRequest', data => {
            this.getSimulatedLocation()
        })
        setInterval(() => {this.getSimulatedLocation()}, 10000)*/


        // REAL GPS
        this.pos = {
            latitude: geoplugin_latitude(),
            longitude: geoplugin_longitude()
        }
        this.far = this.checkHomeNearness() == "far" ? true : false
        setInterval(() => {this.getLocation()}, 5000)
    },
    template: '<div></div>'
}