const AlarmHistory = {
    data: () => ({
        rooms: []
    }),
    template: `
    <v-row justify="center">
    <v-col cols="11" sm="11" md="11">
        <v-expansion-panels accordion>
        <v-expansion-panel
            v-for="room in rooms"
            :key="room._id"
        >
            <v-expansion-panel-header>{{room.name}}</v-expansion-panel-header>
            <v-expansion-panel-content>
                <v-virtual-scroll
                :items="room.alarm_events"
                :item-height="50"
                height="300"
                >
                <template v-slot="{ item }">
                    <v-list-item>
                        <div>{{item.timestamp}}</div>
                    </v-list-item>
                </template>
                </v-virtual-scroll>
            </v-expansion-panel-content>
        </v-expansion-panel>
        </v-expansion-panels>
        </v-col>
    </v-row>`,
    methods: {
        initRooms(){
            axios.get("http://"+this.serverIP+":3000/api/rooms")
              .then(response => {
                  response.data.forEach(room => {
                      var dates = []
                      room.alarm_events.forEach(date =>{
                          var trigger = new Date(date.timestamp)
                          console.log(trigger)
                          dates.push({timestamp: "Attivato il "+trigger.getDate()+"/"+trigger.getMonth()+"/"+trigger.getFullYear()+" alle "+trigger.getHours()+":"+trigger.getMinutes()})
                        })
                      room.alarm_events = dates
                      this.rooms.push(room)
                  })
              }).catch(error => (this.$refs.messagebar.error(error.response.data)))
          }
    },
    computed: {
        eventBus() {
          return this.$store.getters.getEventBus
        }
      },
    mounted(){
        this.eventBus.$emit('updateBreadcrumb', [
            {
                text: 'Home',
                disabled: false,
                href: '/home'
            },{text: '/'},{
                text: 'Allarme',
                disabled: false,
                href: '/alarm'
            },{text: '/'},{
                text: 'Cronologia Allarme',
                disabled: false,
                href: '/alarm/history'
            }])
        this.initRooms()
    }
}