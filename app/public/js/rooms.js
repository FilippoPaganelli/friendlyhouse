const RoomDialog = {
    data: () => ({
        dialog: false,
        name: "",
        nameRules: [v => !!v || 'Name is required'],
        img: "",
        imgRules: [v => !!v || 'URL is required']
    }),
    methods: {
        addNewRoom(){
            this.dialog = false
            axios.post("http://"+this.serverIP+":3000/api/rooms/addRoom", {
                name: this.name,
                img: this.img
            },{headers: {
              'auth-token': localStorage.getItem('auth-token')
            }}).then(response => {
                console.log("Room id: "+response.data)
                this.$emit("addRoom")
            }).catch(error => {
                console.log(error)
            })
        }
    },
    template: `
    <v-row justify="center">
    <v-dialog v-model="dialog" persistent max-width="600px">
      <template v-slot:activator="{ on, attrs }">
        <v-btn fab dark large fixed color="pink" bottom right v-bind="attrs" v-on="on">
            <v-icon>mdi-plus</v-icon>
        </v-btn>
      </template>
      <v-card>
        <v-card-title>
          <span class="headline">Nuova Stanza</span>
        </v-card-title>
        <v-card-text>
          <v-container>
          <v-form
          ref="form"
          lazy-validation
          >
            <v-row>
              <v-col cols="12" sm="12" md="12">
                <v-text-field 
                label="Nome*" 
                required
                hint="Inserisci il nome della nuova Stanza"
                persistent-hint
                :rules="nameRules"
                v-model="name"
                ></v-text-field>
              </v-col>
              <v-col cols="12" sm="12" md="12">
                <v-text-field 
                label="URL Immagine*" 
                required
                hint="Inserisci l'URL dell'immagine per la tua nuova stanza"
                persistent-hint
                :rules="imgRules"
                v-model="img"
                ></v-text-field>
              </v-col>
            </v-row>
            </v-form>
          </v-container>
          <small>*indicates required field</small>
        </v-card-text>
        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="blue darken-1" text @click="dialog = false">CHIUDI</v-btn>
          <v-btn color="blue darken-1" text @click="addNewRoom">SALVA</v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
  </v-row>`
}

const UpdateRoomDialog = {
    props: ['item'],
    data: () => ({
        dialog: false,
        name: "",
        nameRules: [v => !!v || 'Name is required'],
        img: "",
        imgRules: [v => !!v || 'URL is required']
    }),
    methods: {
        updateRoom(id){
            this.dialog = false
            axios.put("http://"+this.serverIP+":3000/api/rooms/updateRoom/"+id, {
                name: this.name,
                img: this.img
            },{headers: {
              'auth-token': localStorage.getItem('auth-token')
            }}).then(response => {
                console.log("Room id: "+response.data)
                this.$emit("updateRoom")
            }).catch(error => {
                console.log(error)
            })
        }
    },
    template: `
    <v-row justify="center">
    <v-dialog v-model="dialog" persistent max-width="600px">
      <template v-slot:activator="{ on, attrs }">
        <v-btn fab dark small color="green" class="ma-5" v-bind="attrs" v-on="on">
            <v-icon>mdi-pencil</v-icon>
        </v-btn>
      </template>
      <v-card>
        <v-card-title>
          <span class="headline">Modifica Stanza</span>
        </v-card-title>
        <v-card-text>
          <v-container>
          <v-form
          ref="form"
          lazy-validation
          >
            <v-row>
              <v-col cols="12" sm="12" md="12">
                <v-text-field 
                :label="item.name"
                required
                hint="Inserisci il nuovo nome della stanza"
                persistent-hint
                :rules="nameRules"
                v-model="name"
                ></v-text-field>
              </v-col>
              <v-col cols="12" sm="12" md="12">
                <v-text-field 
                :label="item.img"
                required
                hint="Inserisci l'URL della nuova immagine per la tua stanza"
                persistent-hint
                :rules="imgRules"
                v-model="img"
                ></v-text-field>
              </v-col>
            </v-row>
            </v-form>
          </v-container>
          <small>*indicates required field</small>
        </v-card-text>
        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn color="blue darken-1" text @click="dialog = false">CHIUDI</v-btn>
          <v-btn color="blue darken-1" text @click="updateRoom(item._id)">SALVA</v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
  </v-row>`
}

const DeleteRoomDialog = {
  props: ['item'],
  data: () => ({
      dialog: false,
      dummyID: null,
      radioGroup: false
  }),
  template: `
  <v-row justify="center">
  <v-dialog v-model="dialog" persistent max-width="600px">
    <template v-slot:activator="{ on, attrs }">
      <v-btn fab dark small color="red" class="ma-5" v-bind="attrs" v-on="on">
          <v-icon>mdi-delete</v-icon>
      </v-btn>
    </template>
    <v-card>
      <v-card-title>
        <span class="headline">Elimina Stanza</span>
      </v-card-title>
      <v-card-text>
      <v-container>
      <v-form>
      <v-radio-group v-model="radioGroup">
        <v-radio
          label="Elimina anche i dispositivi contenuti in questa stanza"
          value="true"
        ></v-radio>
        <v-radio
        label="Disattiva tutti i dispositivi di questa stanza senza eliminarli"
        value="false"
      ></v-radio>
    </v-radio-group>
    </v-form>
    </v-container>
      </v-card-text>
      <v-card-actions>
        <v-spacer></v-spacer>
        <v-btn color="blue darken-1" text @click="dialog = false">CHIUDI</v-btn>
        <v-btn color="blue darken-1" text @click="deleteRoom(item._id)">AVANTI</v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>
  </v-row>`,
  methods: {
      deleteRoom(id){
          if(this.radioGroup == 'false') {
              this.item.devices.forEach(dev => {
                  axios.put("http://"+this.serverIP+":3000/api/rooms/"+id+"/moveDevice/"+dev._id, {
                      newRoom: this.dummyID
                  },{headers: {
                      'auth-token': localStorage.getItem('auth-token')
                  }}).then(response => {
                    axios.delete("http://"+this.serverIP+":3000/api/rooms/deleteRoom/"+id,
                    {headers: {
                      'auth-token': localStorage.getItem('auth-token')
                    }}).then(response => {
                        console.log("Room id: "+response.data)
                        this.$emit("deleteRoom")
                    }).catch(error => {
                        console.log(error)
                    })
                      console.log("Device id deleted: "+response.data)
                  }).catch(error => {
                      console.log(error)
                  })
              })
          } else {
              axios.delete("http://"+this.serverIP+":3000/api/rooms/deleteRoom/"+id,
              {headers: {
                'auth-token': localStorage.getItem('auth-token')
              }}).then(response => {
                  console.log("Room id: "+response.data)
                  this.$emit("deleteRoom")
              }).catch(error => {
                  console.log(error)
              })
          }
      },
      initDummyID(){
          axios.get("http://"+this.serverIP+":3000/api/rooms/dummy")
          .then(response => {
              this.dummyID = response.data[0]._id                    
          }).catch(error => {
              console.log(error)
          })
      }
  },
  mounted(){
    this.initDummyID()
  }
}

const Rooms = {
    components: {
        'message-bar': MessageBar,
        'room-dialog': RoomDialog,
        'update-room': UpdateRoomDialog,
        'delete-room': DeleteRoomDialog
    },
    computed: {
      isAdmin() {
      	return this.$store.getters.isAdmin
      },
      username() {
      	return this.$store.getters.getUsername
      },
      eventBus() {
        return this.$store.getters.getEventBus
      }
    },
    template: `
    <div>
      <v-container fluid>
        <v-layout wrap>
          <div v-for="item in items" :key=item._id>
            <v-flex d-flex>
                <div class="float-left">
                  <v-card class="ma-5" min-width="220" max-width="300" :fullscreen="$vuetify.breakpoint.mobile">
                      <v-img
                      class="white--text align-end"
                      height="100px"
                      v-bind:src=item.img
                      >
                      <v-card-title class="pb-0">{{item.name}}</v-card-title>
                      </v-img>
                      <v-card-subtitle class="pb-0">{{item.description}}</v-card-subtitle>
                    <v-card-text v-if="item.info != 'null'" class="text--primary">
                        <div>{{item.info}}</div>
                    </v-card-text>
                    <v-card-actions class="pa-3">
                        <v-btn color="blue lighten-2" @click="handleRedirect(item.name)" text> APRI </v-btn>
                        <v-btn color="blue lighten-2" v-bind:to=item.link text> AZIONE<br> RAPIDA </v-btn>
                        <update-room v-if="isAdmin == true" :item="item" @updateRoom="initRooms"></update-room>
                        <delete-room v-if="isAdmin == true" :item="item" @deleteRoom="initRooms"></delete-room>
                    </v-card-actions>
                  </v-card>
                </div>
            </v-flex>
          </div>

              <room-dialog v-if="isAdmin == true" @addRoom="initRooms"></room-dialog>

        </v-layout>
      </v-container>
    </div>`,
    data: () => ({
        items: []
    }),
    methods: {
        initRooms(){
          axios.get("http://"+this.serverIP+":3000/api/rooms")
            .then(response => {
                this.items = response.data
                console.log(this.items)
            }).catch(error => (this.$refs.messagebar.error(error.response.data)))
        },
        handleRedirect(room){
          this.$router.push(`/devices/${room}`)
        }
    },
    mounted(){
        this.eventBus.$emit('updateBreadcrumb', [
          {
            text: 'Home',
            disabled: false,
            href: '/home'
        },{text: '/'},{
            text: 'Stanze',
            disabled: false,
            href: '/roomsdevices'
        }])
        this.initRooms()
    }
}