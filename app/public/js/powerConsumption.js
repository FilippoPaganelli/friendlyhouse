const Elettric = {
  computed: {
    eventBus() {
      return this.$store.getters.getEventBus
    }
  },
  methods: {
    loadData: function() {
      //conversion from 0-based
      console.log(this.beginMonth)
      console.log(this.endMonth)
      //let beginMonth = this.beginMonth.getMonth() + 1
      //let endMonth = this.beginMonth.getMonth() + 1

      axios
        .get(`http://${this.serverIP}:3000/api/eletric/global/${this.beginMonth}/${this.endMonth}`,
        {headers: {
            'auth-token': localStorage.getItem('auth-token')
        }})
        .then(response => {
          this.elettricConsumptionData = response.data
          console.log(this.elettricConsumptionData)
          this.totalHours = this.elettricConsumptionData['total_time']
          this.totalHours = (Math.round(this.totalHours * 100) / 100).toFixed(2);

          this.totalWattage = this.elettricConsumptionData['total_wattage']
          this.totalWattage = (Math.round(this.totalWattage * 100) / 100).toFixed(2);

          delete this.elettricConsumptionData['total_wattage']
          delete this.elettricConsumptionData['total_time']


          this.loadGraph()
        })

    },
    loadGraph: function() {
      let labels = Object.keys(this.elettricConsumptionData)
      let data_wattage = Object.entries(this.elettricConsumptionData).map(x =>  x[1].total_wattage)
      let data_time = Object.entries(this.elettricConsumptionData).map(x =>  x[1].total_time)
      this.graph = new Chart(this.graphCtx, {
        type: 'line',
        data: {
          labels: labels,
          datasets: [{
              data: data_wattage,
              label: "Watt",
              borderColor: "#3e95cd",
              fill: true
            }
          ]
        },
        options: {
          title: {
            display: true,
            text: 'Consumo elettrico mese corrente'
          }
        }
      });

    },
    init: function() {
      this.loadData()
      this.graphCtx = document.getElementById('elettricConsumtionGraph').getContext('2d');
    }
  },
  data() {
    return {
      elettricConsumptionData: [],
      totalHours: null,
      totalWattage: null,
      graph: null,
      graphCtx: null,
      beginMonth: new Date().toISOString().substring(0,7),
      endMonth: new Date().toISOString().substring(0,7),
      menu1: false,
      menu2: false
    }
  },
  mounted() {
    this.eventBus.$emit('updateBreadcrumb', [
      {
          text: 'Home',
          disabled: false,
          href: '/home'
      },{text: '/'},{
          text: 'Consumo Elettrico',
          disabled: false,
          href: '/powerconsumption'
      }])
    this.init()
  },
  template: `
    <v-container elevation-12 white mt-5>
      <canvas id="elettricConsumtionGraph" style="width: 100%"></canvas>
      <ul>
        <li>Ore totali: {{totalHours}}</li>
        <li>Consumo totale: {{totalWattage}}</li>
      </ul>


      <template>
        <v-container grid-list-md>
          <v-layout row wrap>
            <v-flex xs12 lg6>
              <v-menu
                v-model="menu1"
                :close-on-content-click="false"
                full-width
                max-width="290"
              >
                <template v-slot:activator="{ on }">
                  <v-text-field
                    :value="beginMonth"
                    clearable
                    label="Mese iniziale"
                    readonly
                    v-on="on"
                  ></v-text-field>
                </template>
                <v-date-picker
                  type="month"
                  v-model="beginMonth"
                  @change="menu1 = false; loadData"
                ></v-date-picker>
              </v-menu>
            </v-flex>

            <v-flex xs12 lg6>
              <v-menu
                v-model="menu2"
                :close-on-content-click="false"
                full-width
                max-width="290"
              >
                <template v-slot:activator="{ on }">
                  <v-text-field
                    :value="endMonth"
                    clearable
                    label="Mese finale escluso"
                    readonly
                    v-on="on"
                  ></v-text-field>
                </template>
                <v-date-picker
                  type="month"
                  v-model="endMonth"
                  @change="menu2 = false; loadData()"
                ></v-date-picker>
              </v-menu>
            </v-flex>
          </v-layout>
        </v-container>
      </template>
    </v-container>
  `
}
