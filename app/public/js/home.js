const Card = {
    components: {
        'message-bar': MessageBar,
    },
    props: ['feature'],
    data: () => ({
        item: {}
    }),
    methods: {
        initUserCard(){
            axios.get("http://"+this.serverIP+":3000/api/features/user/"+this.feature)
                .then(response => {
                    this.item = response.data[0]
                    console.log(response.data[0])
                }).catch(error => (this.$refs.messagebar.error(error.response.data)))
        },
    },
    computed: {
        getMaxWitdth(){
            return window.innerWidth > 600 ? 300 : (window.innerWidth * 0.88)
        }
    },
    mounted(){
        this.initUserCard()
    },
    template: `
        <div class="float-left">
            <v-card class="ma-5" min-width="220" :max-width="getMaxWitdth" height="220">
                <v-img
                class="white--text align-end"
                height="100px"
                v-bind:src=item.img
                >
                <v-card-title class="pb-0">{{item.name}}</v-card-title>
                </v-img>
            
                <v-card-text class="text--primary">
                <div>{{item.description}}</div>
              </v-card-text>
              <v-card-actions>
                <v-btn color="orange" v-bind:to=item.link text> APRI </v-btn>
              </v-card-actions>
            </v-card>
        </div>
    `
}

const Home = {
    components: {
        'card': Card
    },
    data: () => ({
        items: []
    }),
    computed: {
        eventBus() {
            return this.$store.getters.getEventBus
        }
    },
    methods: {
        initFeaturesUser: function(){
            // prendere le funzionalità dal db in base all'utente se è 
            // il tecnico, l'amministratore o uno qualsiasi
            this.items.push({ text: 'Account' })
            this.items.push({ text: 'Contatti' })
            this.items.push({ text: 'StanzeDispositivi' })
            this.items.push({ text: 'ConsumoElettrico' })
            this.items.push({ text: 'Allarme' })
        }
    },
    mounted(){
        this.eventBus.$emit('updateBreadcrumb', [{
            text: 'Home',
            disabled: false,
            href: '/home'
        }, {text: '/'}])
        this.initFeaturesUser()
    },
    template: `
        <v-container fluid>
            <v-layout wrap>
                <div v-for="item in items" :key=item.text>
                    <v-flex d-flex>
                        <card v-bind:feature="item.text" :fullscreen="$vuetify.breakpoint.mobile"></card>
                    </v-flex>
                </div>
            </v-layout>
        </v-container>
    `
}