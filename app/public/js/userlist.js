const SignupDialog = {
	components: {
		'message-bar': MessageBar
	},
	template: `
		<v-dialog v-model="dialog" persistent max-width="600px">
			<message-bar ref="messageBar"></message-bar>

			<template v-slot:activator="{ on, attrs }">
				<v-btn
		            dark
		            fab
		            bottom
		            right
		            large
		            fixed
		            color="pink"
					v-bind="attrs"
					v-on="on"
				>
					<v-icon>mdi-plus</v-icon>
				</v-btn>
			</template>

			<v-card>
				<v-card-title>
					<span class="headline">Creazione nuovo utente</span>
				</v-card-title>
				<v-card-text>

					<v-form
					ref="form"
					lazy-validation
					>
						<v-text-field
							label="Username"
							prepend-icon="mdi-account-circle"
							v-model="username"
							:rules="usernameRules"
							required
						/>
						<v-text-field
							:type="showPassword ? 'text': 'password' "
							label="Password"
							prepend-icon="mdi-lock"
							:append-icon="showPassword ? 'mdi-eye' : 'mdi-eye-off'"
							@click:append="showPassword = !showPassword"
							v-model="password"
							:rules="passwordRules"
							required
						/>
						<v-radio-group v-model="role" required>
							<v-radio
								label="Utente"
								value="user"
							>
							</v-radio>
							<v-radio
								label="Amministratore"
								value="admin"
							>
							</v-radio>

						</v-radio-group>
					</v-form>

				</v-card-text>

				<v-card-actions>
					<v-spacer></v-spacer>
						<v-btn color="blue darken-1" text @click="closeDialog()">Chiudi</v-btn>
						<v-btn color="blue darken-1" text @click="handleSubmit">Registra</v-btn>
				</v-card-actions>
			</v-card>
		</v-dialog>
	`,
	data() {
		return {
			username: "",
			usernameRules: [
		        v => !!v || 'Name is required',
		    ],
			password: "",
			passwordRules: [
				v => !!v || 'Password is required',
				v => (v && v.length >= 8) || 'Password must be less than 8 characters',
			],
			role: 'user',
			showPassword: false,
			snackbar: false,
			snackbarMessage: '',
			snackbarColor: '',
			dialog: false
		}
	},
	methods: {
		closeDialog: function() {
			this.dialog = false
		},
		handleSubmit: function() {
			if (this.$refs.form.validate()) {
				axios.post("http://"+this.serverIP+":3000/api/user/register", {
					username: this.username,
					password: this.password,
					role: this.role
				}).then(response => {
					this.$refs.messageBar.success("Utente registrato!");
					this.$emit('userUpdate');
					this.closeDialog()
				}).catch(error => {
					console.log(error)
					this.$refs.messageBar.error(error.response.data)
				});
			} else {
				console.log("is not valid")
			}
		}
	}
}

const EliminationDialog = {
	props: ['username'],
	template: `
		<v-dialog v-model="dialog" persistent max-width="600px">
			<template v-slot:activator="{ on, attrs }">
				<v-btn
					color="error"
					dark
					v-bind="attrs"
					v-on="on"
                    icon
				>
					<v-icon>mdi-account-remove</v-icon>
				</v-btn>
			</template>
		<v-card>
			<v-card-title class="headline">Conferma eliminazione</v-card-title>
			<v-card-text>Vuoi eliminare {{username}}?</v-card-text>
			<v-card-actions>
				<v-spacer></v-spacer>
				<v-btn color="success" @click="dialog = false">Chiudi</v-btn>
				<v-btn color="error" @click="deleteUser(username)">Elimina</v-btn>
		  	</v-card-actions>
		</v-card>
	  </v-dialog>
	`,
	data() {
		return {
			dialog: false
		}
	},
	methods: {
		deleteUser: function(username) {
			this.dialog = false
			console.log(username)

			axios.delete(
				`http://${this.serverIP}:3000/api/users/${username}`,
				{headers: {
					'auth-token': localStorage.getItem('auth-token')
				}})
			.then(response => {
				console.log("utente eliminato")
				this.$emit('userUpdate');
			})
			.catch(error => {
				console.log(error)
			});
		},
		show: function () {
			this.dialog = true
		}
	}
}

const PromoteDialog = {
	props: ['username'],
	template: `
		<v-dialog v-model="dialog" persistent max-width="600px">
			<template v-slot:activator="{ on, attrs }">
				<v-btn
					color="warning"
					dark
					v-bind="attrs"
					v-on="on"
                    icon
				>
					<v-icon>mdi-account-star</v-icon>
				</v-btn>
			</template>
		<v-card>
			<v-card-title class="headline">Conferma promozione</v-card-title>
			<v-card-text>Vuoi promuovere {{username}} ad amministratore?</v-card-text>
			<v-card-actions>
				<v-spacer></v-spacer>
				<v-btn color="success" @click="dialog = false">Chiudi</v-btn>
				<v-btn color="error" @click="promote(username)">Conferma</v-btn>
		  	</v-card-actions>
		</v-card>
	  </v-dialog>
	`,
	data() {
		return {
			dialog: false
		}
	},
	methods: {
		promote: function(name) {
			axios.put(
				`http://${this.serverIP}:3000/api/users/${name}/toadmin`,
				{},
				{headers: {
					'auth-token': localStorage.getItem('auth-token')
				}})
			.then(response => {
				console.log("utente aggiornato")
				this.$emit('userPromoted');
				this.dialog = false
			})
			.catch(error => {
				console.log(error)
			});
		},
		show: function () {
			this.dialog = true
		}
	}
}

const RemoveAdminDialog = {
	props: ['username'],
	template: `
		<v-dialog v-model="dialog" persistent max-width="600px">
			<template v-slot:activator="{ on, attrs }">
				<v-btn
					color="warning"
					dark
					v-bind="attrs"
					v-on="on"
                    icon
				>
					<v-icon>mdi-account-multiple-minus</v-icon>
				</v-btn>
			</template>
		<v-card>
			<v-card-title class="headline">Rimuovi amministratore</v-card-title>
			<v-card-text>Vuoi rimuovere {{username}} dagli amministratori?</v-card-text>
			<v-card-actions>
				<v-spacer></v-spacer>
				<v-btn color="success" @click="dialog = false">Chiudi</v-btn>
				<v-btn color="error" @click="remove(username)">Rimuovi</v-btn>
		  	</v-card-actions>
		</v-card>
	  </v-dialog>
	`,
	data() {
		return {
			dialog: false
		}
	},
	methods: {
		remove: function(name) {
			axios.put(
				`http://${this.serverIP}:3000/api/users/${name}/touser`,
				{},
				{headers: {
					'auth-token': localStorage.getItem('auth-token')
				}})
			.then(response => {
				console.log("utente aggiornato")
				this.$emit('adminRemoved');
				this.dialog = false
			})
			.catch(error => {
				console.log(error)
			});
		},
		show: function () {
			this.dialog = true
		}
	}
}

const UserList = {
	components: {
		'message-bar': MessageBar,
		'signup-dialog': SignupDialog,
		'elimination-dialog': EliminationDialog,
		'promote-dialog': PromoteDialog,
		'remove-dialog': RemoveAdminDialog
	},
	template: `
	<v-container>
		<div class="mt-5">

			<div class="text-h2 mb-5">
            	Utente attuale
            </div>

			<v-card class="mx-5 elevation-5">
				<v-avatar color="success">
					<span class="white--text headline">{{this.username | getInitials}}</span>
				</v-avatar>

				<v-card-title>{{this.username}}</v-card-title>
				<v-card-subtitle>
					<p v-if="this.isAdmin">Amministratore</p>
					<p v-else>Utente</p>
				</v-card-subtitle>

			</v-card>

			<div class="text-h2 mt-5">
            	Altri utenti:
            </div>

			<signup-dialog v-if="isAdmin" @userUpdate="listUsers"></signup-dialog>
			<v-row wrap>
				<v-col v-for="user in users" :key="users._id" cols="12" md="6" xl="4">
					<v-card class="mx-5 elevation-5">
						<v-avatar color="success">
					    	<span class="white--text headline">{{user.username | getInitials}}</span>
					    </v-avatar>

						<v-card-title>{{user.username}}</v-card-title>
						<v-card-subtitle>
							<p v-if="user.isAdmin">Amministratore</p>
							<p v-else>Utente</p>
						</v-card-subtitle>

						<v-card-actions>
              <v-btn icon @click="openChat(user.username)"><v-icon>mdi-message-text</v-icon></v-btn>
              <v-spacer></v-spacer>
              <div v-if="isAdmin">
                <promote-dialog v-if="!user.isAdmin" :username="user.username" @userPromoted="userPromoted(user.username)"></promote-dialog>
                <remove-dialog v-if="user.isAdmin" :username="user.username" @adminRemoved="adminRemoved(user.username)"></remove-dialog>
                <elimination-dialog :username="user.username" @userUpdate="removeUser(user.username)"></elimination-dialog>
              </div>
						</v-card-actions>
					</v-card>
				</v-col>
			</v-row>
		</div>
	</v-container>
	`,
	data() {
		return {
			users: [],
		}
	},
	computed: {
      isAdmin() {
      	return this.$store.getters.isAdmin;
      },
      username() {
      	return this.$store.getters.getUsername;
	  },
	  eventBus() {
		return this.$store.getters.getEventBus
	  }
    },
	methods: {
		adminRemoved: function(username) {
			this.users.find(user => user.username === username).isAdmin = false
		},
		userPromoted: function(username) {
			this.users.find(user => user.username === username).isAdmin = true
		},
		removeUser: function(username){
			this.users = this.users.filter(user => user.username !== username)
		},
		listUsers: function () {
			axios.get(
				`http://${this.serverIP}:3000/api/users`,
				{headers: {
					'auth-token': localStorage.getItem('auth-token')
				}})
			.then(response => {
				this.users = response.data
				this.users = this.users.filter(user => user.username !== this.username)
				console.log(this.users)
			}).catch(error => /*(this.$refs.messagebar.error(error.response.data))*/{});

		},
    openChat: function(to)  {
      router.push(`/chat/${to}`)
    },
		init: function(){
			this.username = localStorage.getItem("username")
			this.listUsers();
		}
	},
	filters: {
		getInitials: function(name) {
			return name
				    .split(' ')
				    .map(([firstLetter]) => firstLetter)
				    .filter((_, index, array) => index === 0 || index === array.length - 1)
				    .join('')
				    .toUpperCase();
		}
	},
	//eseguito quando è pronto per essere mostrato
	mounted(){
		this.eventBus.$emit('updateBreadcrumb', [
			{
				text: 'Home',
				disabled: false,
				href: '/home'
			},{text: '/'},{
				text: 'Elenco Utenti',
				disabled: false,
				href: '/userlist'
			}])
		this.init()
	}
}
