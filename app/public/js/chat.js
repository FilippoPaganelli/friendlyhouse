const Chat = {

    template: `

        <v-container v-if="this.ready && this.valid" fill-height style="">
          <v-card class="mt-5 elevation-12" style="width:100%">
            <v-toolbar
              color="primary"
              dark
              flat
            >
              <v-toolbar-title>Stai chattando con: {{to}}</v-toolbar-title>
            </v-toolbar>


             <v-card-text style="min-height:60vh">

             <v-list
                style="height: 60vh; max-height: 60vh"
                class="overflow-y-auto"
              >
                <template v-for="msg in messages">
                  <v-list-item :key="msg.id">
                    <v-list-item-content>
                      <v-list-item-title>{{msg.from}}: <br>{{msg.content}}</v-list-item-title>
                    </v-list-item-content>
                  </v-list-item>

                </template>
              </v-list>
            </v-card-text>

            <v-card-actions>
              <v-text-field v-model="currentMessage" />
              <v-btn color="secondary" fab small @click="sendMsg"> <v-icon sall>mdi-send</v-icon> </v-btn>
            </v-card-actions>
          </v-card>
        </v-container>
    `,
    data() {
        return {
            //usato nel template per fare un bind
            //quando viene modificato questo viene modificata
            //anche la visualizzazione
            messages: [],
            to: "",
            valid: false,
            ready: false,
            currentMessage: ""
        }
    },
    computed: {
        username() {
          return this.$store.getters.getUsername;
        },
        eventBus() {
          return this.$store.getters.getEventBus
        }
    },
    methods: {
        init: function(){
          this.eventBus.$emit('updateBreadcrumb', [
            {
              text: 'Home',
              disabled: false,
              href: '/home'
            },{text: '/'},{
              text: 'Utenti',
              disabled: false,
              href: '/userlist'
            },{text: '/'},{
              //text: 'Chat con '+this.$route.params.to,
              text: "Chat",
              disabled: false,
              href: '/chat/'+this.$route.params.to
            }])
          this.to = this.$route.params.to
          this.loadMessages()
          this.eventBus.$on('ResponseReceived', msg => {
            console.log("response recevied from event", msg)
            this.loadMessages()
          })
          axios
            .put(
                `http://${this.serverIP}:3000/api/notifications/command/users/${this.username}/readmessages/${this.to}`,
                {},
                {headers: {
                    'auth-token': localStorage.getItem('auth-token')
                }})
        },
        loadMessages: function() {
          axios
              .get(
                  `http://${this.serverIP}:3000/api/chat/message/${this.username}/with/${this.to}`,
                  {headers: {
                      'auth-token': localStorage.getItem('auth-token')
                  }})
              .then(
                  response => {
                      this.valid = true
                      this.ready = true
                      this.messages = response.data
                      console.log("valid")
                      //this.renderMessages()
                  } ,
                  error => {
                      this.valid = false
                      this.ready = true
                      console.log("invalid")
                  }
              )
        },
        sendMsg: function() {
          console.log("send message")
            axios
                .post(
                    `http://${this.serverIP}:3000/api/chat/message`,
                    {
                        "from": this.username,
                        "to": this.to,
                        "content": this.currentMessage
                    },
                    {headers: {
                        'auth-token': localStorage.getItem('auth-token')
                    }})
                .then(response => {
                    this.currentMessage = ""
                    console.log("sent")
                    this.init()
                })
        }
    },
    //eseguito quando è pronto per essere mostrato
    mounted(){
      this.init()
    },
    //prima di uscire dalla rotta rimuvoe gli eventi
    //in questo modo solo la rotta attiva attualmente intercetta gli eventi
    beforeRouteLeave(to, from, next) {
      this.eventBus.$off('ResponseReceived' )
      next();
    }
}
