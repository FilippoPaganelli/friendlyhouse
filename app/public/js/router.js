const router = new VueRouter({
    mode: 'history',
    routes: [
		/*
			name: Il contenuto del campo `to` del navlink che viene cliccato
			path: percorso in cui si va
			component: componente che viene caricato dal path indicato
    */
      { path: '/', name: 'Entry', component: Entry },
      { path: '/home', name: 'Home', component: Home , meta: {breadcrumb: 'Home'} },
      { path: '/alarm', name: 'Alarm', component: Alarm , meta: {breadcrumb: 'Allarme'}},
      { path: '/alarm/history', name: 'AlarmHistory', component: AlarmHistory, meta: {breadcrumb: 'Cronologia Allarme'} },
      { path: '/roomsdevices', name: 'StanzeDispositivi', component: Rooms, meta: {breadcrumb: 'Stanze'}},
      { path: '/devices/:room', name: 'Devices', component: Devices, meta: {breadcrumb: 'Dispositivi'}},
      { path: '/signup', name: 'Registrati', component: Signup },
      { path: '/login', name: 'Login', component: Login},
      { path: '/testtoken', name: 'TestToken', component: TestToken},
      { path: '/userlist', name: 'Elenco Utenti', component: UserList, meta: {breadcrumb: 'Elenco Utenti'}},
      { path: '/chat/:to', name: 'Chat', component: Chat, meta: {breadcrumb: 'Chat'}},
      { path: '/notifications', name: 'Notifications', component: Notifications, meta: {breadcrumb: 'Notifiche'}},
      { path: '/powerconsumption', name: 'ConsumoElettrico', component: Elettric, meta: {breadcrumb: 'Consumo Elettrico'}},
      { path: '/404', component: NotFound },
      { path: '*', redirect: '/404' }
    ]
})
