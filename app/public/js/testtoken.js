//TODO: creare un componente dedicato alla snackbar?
const TestToken = {
	template: `
	<v-app>
		<v-main>
			<v-container
	          class="fill-height"
	          fluid
	        >
		        <v-row
		        	align="center"
					justify="center"
		        >
		            <v-col
		              cols="12"
		              sm="8"
		              md="4"
		            >
						<v-btn @click="handleAdmin"> Test Admin </v-btn>
						<v-btn @click="handleUser"> Test User </v-btn>
					</v-col>
	 			</v-row>
   			</v-container>
		</v-main>
	</v-app>
	`,
	data() {
		return {

		}
	},
	methods: {
		handleAdmin() {
			console.log("handleAdmin")
			console.log(localStorage.getItem('auth-token'))
			axios.get(
				"http://"+this.serverIP+":3000/api/user/testtokenAdmin",
				{headers: {
					'auth-token': localStorage.getItem('auth-token')
				}}
			)
			.then(response => {
				console.log(response)
			}).catch(error => {
				console.log(error)
			});
		},
		handleUser() {
			console.log("handleUser")
			console.log(localStorage.getItem('auth-token'))
			axios.get(
				"http://"+this.serverIP+":3000/api/user/testtoken",
				{headers: {
					'auth-token': localStorage.getItem('auth-token')
				}}
			)
			.then(response => {
				console.log(response)
			}).catch(error => {
				console.log(error)
			});		},
		init() {
		}
	},
	mounted(){
		this.init()
	}
}