const request = require('supertest')
var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var cors = require('cors')
var path = require('path')
var env = require('dotenv')
env.config()
global.appRoot = path.resolve(__dirname)
app.use(bodyParser.urlencoded( {extended: false }))
app.use(bodyParser.json())
app.use(cors())
app.use('/static', express.static(__dirname + '/public'))
var routes = require('../src/routes/routes')
routes(app)

jest.setTimeout(30000);

//Middleware
app.use(express.json())

var server = app.listen(process.env.PORT, process.env.IP_ADDRESS, function() {
    console.log('Node Server listening on port '+process.env.PORT)
})

let adminToken

beforeAll(async () => {
    console.log("Before all")
    await mongoose.connect(process.env.DB_CONNECT_TEST, {
          useNewUrlParser: true,
          useFindAndModify: true,
          useUnifiedTopology: true})
      .then(async () => {
        console.log(`MongoDB Connected: ${process.env.DB_CONNECT_TEST}`)
        mongoose.connection.db.dropCollection('Alarm', function(err, result) {})
        mongoose.connection.db.dropCollection('Users', function(err, result) {})
        console.log("alarm collection dropped")

        await request(app)
            .post('/api/user/register')
            .send({
                'username': 'administrator',
                'password': 'password',
                'role': 'admin'
            })

        await request(app)
            .post('/api/user/login')
            .send({
                'username': 'administrator',
                'password': 'password',
            }).then(response => {
                adminToken = response.header['auth-token']
            })
      }).catch((err) => console.log(err))
})

describe('Pin creation', () => {
    it('should be possibile to create a pin', async() => {
        await request(app)
            .post('/api/alarm/add')
            .set({'auth-token': adminToken})
            .send({
                'pin': "1234"
            })
            .expect(201)
    })

    it('should be impossibile to create a pin without admin permissions', async() => {
        await request(app)
            .post('/api/alarm/add')
            .send({
                'pin': "1234"
            })
            .expect(401)
    })

    it('should be impossibile to create a pin without pin', async() => {
        await request(app)
            .post('/api/alarm/add')
            .set({'auth-token': adminToken})
            .send({
                'pin': ""
            })
            .expect(400)
    })
    
    it('should be possibile to update a pin', async() => {
        await request(app)
            .put('/api/alarm/update')
            .set({'auth-token': adminToken})
            .send({
                'pin': "0000"
            })
            .expect(200)
    })

    it('should be impossibile to update a pin without admin permissions', async() => {
        await request(app)
            .put('/api/alarm/update')
            .send({
                'pin': "0000"
            })
            .expect(401)
    })

    it('should be impossibile to update a pin without a new pin', async() => {
        await request(app)
            .put('/api/alarm/update')
            .set({'auth-token': adminToken})
            .send({
                'pin': ""
            })
            .expect(400)
    })

})

describe('Pin validation', () => {
    it('should be possibile to validate a correct pin', async() => {
        await request(app)
            .post('/api/alarm/verify')
            .set({'auth-token': adminToken})
            .send({
                'pin': "0000"
            })
            .expect(200)
    })

    it('should be impossibile to validate an incorrect pin', async() => {
        await request(app)
            .post('/api/alarm/verify')
            .set({'auth-token': adminToken})
            .send({
                'pin': "1234"
            })
            .expect(400)
    })

    it('should be impossibile to validate an correct pin without user token', async() => {
        await request(app)
            .post('/api/alarm/verify')
            .send({
                'pin': "0000"
            })
            .expect(401)
    })
})

afterAll(() => {
    // mongodb middleware tear down
    mongoose.connection.close()

    // express server tear down
    server.close()
})