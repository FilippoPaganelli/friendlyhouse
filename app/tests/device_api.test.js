const request = require('supertest')
var express = require('express')
var http = require('http')
var app = express()
var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var cors = require('cors')
var path = require('path')
var env = require('dotenv')
env.config()
global.appRoot = path.resolve(__dirname);
app.use(bodyParser.urlencoded( {extended: false }))
app.use(bodyParser.json())
app.use(cors())
app.use('/static', express.static(__dirname + '/public'));
var routes = require('../src/routes/routes')
const { response } = require('express')
routes(app)

//Middleware
app.use(express.json())

var server = app.listen(process.env.PORT, process.env.IP_ADDRESS, function() {
    console.log('Node Server listening on port '+process.env.PORT)
})

let adminToken
var roomName = "stanza"
let roomID
var fakeID = mongoose.Types.ObjectId()
let deviceID

beforeAll(async () => {
    console.log("Before all")
    await mongoose.connect(process.env.DB_CONNECT_TEST, {
          useNewUrlParser: true,
          useFindAndModify: true,
          useUnifiedTopology: true})
      .then(() => {
        console.log(`MongoDB Connected: ${process.env.DB_CONNECT_TEST}`)
        mongoose.connection.db.dropCollection('Rooms', function(err, result) {});
        console.log("rooms collection dropped")
      })
      .catch((err) => console.log(err))

    // creazione token admin
    await request(app)
        .post('/api/user/register')
        .send({
        'username': 'administratoree',
        'password': 'password',
        'role': 'admin'
    })

    // get token admin
    await request(app)
    .post('/api/user/login')
    .send({
      'username': 'administratoree',
      'password': 'password',
    })
    .then(response => {
      adminToken = response.header['auth-token']
    })

    // creazione stanza
    await request(app)
    .post('/api/rooms/addRoom')
    .set({'auth-token': adminToken})
    .send({
        'name': roomName,
        'img': 'https://unsplash.com/photos/KxCAa7wpzu0/download?force=true&w=1920'
    })
    .then(response => {
        roomID = response.body['room']
    })
})


describe('Device creation', () => {
    it('should be possibile to add a new device in a room', async() => {
        await request(app)
            .put('/api/rooms/'+roomID+'/addDevice')
            .set({'auth-token': adminToken})
            .send({
                'name': 'device',
                'img': 'https://unsplash.com/photos/KxCAa7wpzu0/download?force=true&w=1920',
                'model': 'XHK837',
                'category': 'lamp',
                'watt': 3
            })
            .expect(201)
            .then(response => {
                deviceID = response.body['devices'][0]['_id']
            })
    })

    it('should be impossibile to add a new device in a room that does not exist', async() => {
        await request(app)
            .put('/api/rooms/'+fakeID+'/addDevice')
            .set({'auth-token': adminToken})
            .send({
                'name': 'device',
                'img': 'https://unsplash.com/photos/KxCAa7wpzu0/download?force=true&w=1920',
                'model': 'XHK837',
                'category': 'lamp',
                'watt': 3
            })
            .expect(400) 
    })

    it('should be impossibile to add a new device in a room without admin permissions', async() => {
        await request(app)
            .put('/api/rooms/'+roomID+'/addDevice')
            .send({
                'name': 'device',
                'img': 'https://unsplash.com/photos/KxCAa7wpzu0/download?force=true&w=1920',
                'model': 'XHK837',
                'category': 'lamp',
                'watt': 3
            })
            .expect(401) 
    })

    it('should be impossibile to add a new device not complete in a room', async() => {
        await request(app)
            .put('/api/rooms/'+roomID+'/addDevice')
            .set({'auth-token': adminToken})
            .send({
                'img': 'https://unsplash.com/photos/KxCAa7wpzu0/download?force=true&w=1920',
                'model': 'XHK837',
                'category': 'lamp',
                'watt': 3
            })
            .expect(400)
    })
})

describe('Device update', () => {
    it('should be possibile to update a device', async() => {
        await request(app)
            .put('/api/rooms/'+roomID+'/updateDevice/'+deviceID)
            .set({'auth-token': adminToken})
            .send({
                'name': 'devicedevice',
                'img': 'https://unsplash.com/photos/KxCAa7wpzu0/download?force=true&w=1920'
            })
            .expect(200)
    })

    it('should be impossibile to update a device without admin permissions', async() => {
        await request(app)
            .put('/api/rooms/'+roomID+'/updateDevice/'+deviceID)
            .send({
                'name': 'device',
                'img': 'https://unsplash.com/photos/KxCAa7wpzu0/download?force=true&w=1920'
            })
            .expect(401)
    })

    it('should be possibile to update a device without new name/img', async() => {
        await request(app)
            .put('/api/rooms/'+roomID+'/updateDevice/'+deviceID)
            .set({'auth-token': adminToken})
            .send({
                'name': 'device'
            })
            .expect(200)
    })

    it('should be possibile to update the state of a device', async() => {
        await request(app)
            .put('/api/rooms/'+roomID+'/updateDeviceState/'+deviceID)
            .set({'auth-token': adminToken})
            .send({
                state: "true"    
            })
            .expect(200)
    })

})

afterAll(() => {
    // mongodb middleware tear down
    mongoose.connection.close()

    // express server tear down
    server.close()
})