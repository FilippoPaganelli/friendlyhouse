const request = require('supertest')
var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var cors = require('cors')
var path = require('path')
var env = require('dotenv')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
env.config()
global.appRoot = path.resolve(__dirname);
app.use(bodyParser.urlencoded( {extended: false }))
app.use(bodyParser.json())
app.use(cors())
app.use('/static', express.static(__dirname + '/public'));
var routes = require('../src/routes/routes')
routes(app)

//Middleware
app.use(express.json())

app.listen(process.env.PORT, process.env.IP_ADDRESS, function() {
    console.log('Node Server listening on port '+process.env.PORT)
})


let tokenMario;

jest.setTimeout(30000);

beforeAll(async () => {
  console.log("Before all")
  await mongoose.connect(process.env.DB_CONNECT_TEST, {
        useNewUrlParser: true,
        useFindAndModify: true,
        useUnifiedTopology: true})
    .then( async () => {
      console.log(`MongoDB Connected: ${process.env.DB_CONNECT_TEST}`)

      mongoose.connection.db.dropCollection('Users', function(err, result) {});
      mongoose.connection.db.dropCollection('Messages', function(err, result) {});


      await request(app)
        .post('/api/user/register')
        .send({
          'username': "mario",
          'password': "password",
          'role': 'user'
        }).expect(201)
      await request(app)
        .post('/api/user/register')
        .send({
          'username': "luigi",
          'password': "password",
          'role': 'user'
        }).expect(201)

      await request(app)
        .post('/api/user/register')
        .send({
          'username': "gianfrancioschio",
          'password': "password",
          'role': 'user'
        }).expect(201)

      await request(app)
        .post('/api/user/login')
        .send({
          'username': "mario",
          'password': "password"
        })
        .expect(200)
        .then(response => {
          tokenMario = response.header['auth-token']
          console.log(tokenMario)
        })
    })
    .catch((err) => console.log(err)
)});


describe("Correct behavior", () => {

  it ("should be possibile to send a message to another user", async () => {

    await request(app)
      .post('/api/chat/message')
      .set({'auth-token': tokenMario})
      .send({
        'from': "mario",
        'to': 'luigi',
        'content': "the brown fox jumps over the lazy dog"
      })
      .expect(201)
      .then(response => {
        const body = response.body
        expect(body).toHaveProperty('from')
        expect(body).toHaveProperty('to')
        expect(body).toHaveProperty('content')
        expect(body).toHaveProperty('timestamp')
        expect(body['from']).toBe('mario')
        expect(body['to']).toBe('luigi')
      })
  })

  it ("should be possibile to get messages with another user", async () => {

    await mongoose.connection.db.dropCollection('Messages', function(err, result) {});

    await request(app)
      .post('/api/chat/message')
      .set({'auth-token': tokenMario})
      .send({
        'from': "mario",
        'to': 'luigi',
        'content': "Message 1"
      })

    await request(app)
      .post('/api/chat/message')
      .set({'auth-token': tokenMario})
      .send({
        'from': "mario",
        'to': 'luigi',
        'content': "Message 2"
      })

    await request(app)
      .get('/api/chat/message/mario/to/luigi')
      .set({'auth-token': tokenMario})
      .expect(200)
      .then(response => {
        response.body.forEach(message => {
          expect(message).toHaveProperty('from')
          expect(message).toHaveProperty('to')
          expect(message).toHaveProperty('content')
          expect(message['from']).toBe('mario')
          expect(message['to']).toBe('luigi')
        })
      })
  })
})

describe("Scorrect behavivour", () => {
  it ("Should not be possibile to send a message without providing an auth token", async () => {
    await request(app)
      .post('/api/chat/message')
      .send({
        'from': "mario",
        'to': 'luigi',
        'content': "the brown fox jumps over the lazy dog"
      })
      .expect(401)
  })
  it ("Should not be possibile to send a message with a different username", async () => {
    await request(app)
      .post('/api/chat/message')
      .set({'auth-token': tokenMario})
      .send({
        'from': "luigi",
        'to': 'mario',
        'content': "the brown fox jumps over the lazy dog"
      })
      .expect(401)
  })
  it ("Should not be possibile to send a message to a non-existing user", async() => {
    await request(app)
      .post('/api/chat/message')
      .set({'auth-token': tokenMario})
      .send({
        'from': "mario",
        'to': 'hope',
        'content': "the brown fox jumps over the lazy dog"
      })
      .expect(409)
  })
  it ("Should not be possibile to send a message to yourself", async () => {
    await request(app)
      .post('/api/chat/message')
      .set({'auth-token': tokenMario})
      .send({
        'from': "mario",
        'to': 'mario',
        'content': "sorry, I needed to talk to someone and you seem like the only smart person in this house"
      })
      .expect(409)
  })
  it ("Should not be possibile to get messagages without providing an auth token", async() => {
    await request(app)
      .get('/api/chat/message/mario/to/luigi')
      .expect(401)
  })
  it ("Should not be posisbile to get messages of another user", async () => {
    await request(app)
      .get('/api/chat/message/luigi/to/mario')
      .set({'auth-token': tokenMario})
      .expect(401)

  })
  it ("Should not be possibile to get messages with a non-existing user", async() => {
    await request(app)
      .get('/api/chat/message/mario/to/mew')
      .set({'auth-token': tokenMario})
      .expect(409)
  })
})
