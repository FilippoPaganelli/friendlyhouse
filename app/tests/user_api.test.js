const request = require('supertest')
var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var cors = require('cors')
var path = require('path')
var env = require('dotenv')
env.config()
global.appRoot = path.resolve(__dirname);
app.use(bodyParser.urlencoded( {extended: false }))
app.use(bodyParser.json())
app.use(cors())
app.use('/static', express.static(__dirname + '/public'));
var routes = require('../src/routes/routes')
routes(app)

//Middleware
app.use(express.json())

app.listen(process.env.PORT, process.env.IP_ADDRESS, function() {
    console.log('Node Server listening on port '+process.env.PORT)
})


beforeAll(async () => {
  console.log("Before all")
  await mongoose.connect(process.env.DB_CONNECT_TEST, {
        useNewUrlParser: true,
        useFindAndModify: true,
        useUnifiedTopology: true})
    .then(() => {
      console.log(`MongoDB Connected: ${process.env.DB_CONNECT_TEST}`)
      mongoose.connection.db.dropCollection('Users', function(err, result) {});
      console.log("user collection dropped")
    })
    .catch((err) => console.log(err)
)});

/*
beforeEach(() => {
  console.log("before each")
});

afterAll(() => {
  console.log("after all")
});

afterAll(() => {
  console.log("after each")
});
*/
let userToken;
let adminToken;

describe('User creation', () => {
  it('should be possible to create a user', async() => {
    await request(app)
      .post('/api/user/register')
      .send({
        'username': 'user',
        'password': 'password',
        'role': 'user'
      })
      .expect(201)
  })
  it('should be possible to create a user - 2', async() => {
    await request(app)
      .post('/api/user/register')
      .send({
        'username': 'user2',
        'password': 'password',
        'role': 'user'
      })
      .expect(201)
  })
  it('should be possible to create a user - 3', async() => {
    await request(app)
      .post('/api/user/register')
      .send({
        'username': 'user3',
        'password': 'password',
        'role': 'user'
      })
      .expect(201)
  })
  it('should be possible to create an admin', async() => {
    await request(app)
      .post('/api/user/register')
      .send({
        'username': 'administrator',
        'password': 'password',
        'role': 'admin'
      })
      .expect(201)
  })
  it('a user should have at least an 8 character password', async() => {
    await request(app)
      .post('/api/user/register')
      .send({
        'username': 'usershortpassword',
        'password': 'short',
        'role': 'user'
      })
      .expect(400)  
    })
})


describe('User access', () => {
  it('a user should be able to login', async() => {
    await request(app)
      .post('/api/user/login')
      .send({
        'username': 'user',
        'password': 'password',
      })
      .expect(200)
      .then(response => {
        expect(response.header).toHaveProperty('auth-token')
        userToken = response.header['auth-token']
        console.log(userToken)
      })

  })
  it('an admin should be able to login', async() => {
    await request(app)
      .post('/api/user/login')
      .send({
        'username': 'administrator',
        'password': 'password',
      })
      .expect(200)
      .then(response => {
        expect(response.header).toHaveProperty('auth-token')
        adminToken = response.header['auth-token']
      })
  })
})
 

describe("User permission", () => {
  it("shoud be possibile to test a user's token", async() => {
    await request(app)
      .get('/api/user/testtoken')
      .set({'auth-token': userToken})
      .expect(200)

  })
  it("a user should be able to list other users", async() => {
    await request(app)
      .get('/api/users')
      .set({'auth-token': userToken})
      .expect(200)
      .then(response => {
        response.body.forEach(user => {
          expect(user).toHaveProperty('_id')
          expect(user).toHaveProperty('username')
          expect(user).toHaveProperty('isAdmin')
        })
      })
  })
})


describe("Admin permissions", () => {
  it("shold be posisbile to verify its token", async() => {
    await request(app)
      .get('/api/user/testtokenAdmin')
      .set({'auth-token': adminToken})
      .expect(200)
  })
  
  it("should be able to promote a user", async() => {
    await request(app)
      .put('/api/users/user/toadmin')
      .set({'auth-token': adminToken})
      .expect(200)
      .then(response => {
        expect(response.body).toHaveProperty('_id')
        expect(response.body).toHaveProperty('username')
        expect(response.body).toHaveProperty('isAdmin')
        expect(response.body['isAdmin']).toBe(true )
      })

  })
  
  it("should be able to remove another admin from the admins", async() => {
    await request(app)
      .put('/api/users/user/toUser')
      .set({'auth-token': adminToken})
      .expect(200)
      .then(response => {
        expect(response.body).toHaveProperty('_id')
        expect(response.body).toHaveProperty('username')
        expect(response.body).toHaveProperty('isAdmin')
        expect(response.body['isAdmin']).toBe(false )
      })
  })
  
  it("should be able to delete a user", async() => {
    await request(app)
      .delete('/api/users/user2')
      .set({'auth-token': adminToken})
      .expect(204)
  })  
})
