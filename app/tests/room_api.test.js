const request = require('supertest')
var express = require('express')
var http = require('http')
var app = express()
var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var cors = require('cors')
var path = require('path')
var env = require('dotenv')
env.config()
global.appRoot = path.resolve(__dirname);
app.use(bodyParser.urlencoded( {extended: false }))
app.use(bodyParser.json())
app.use(cors())
app.use('/static', express.static(__dirname + '/public'));
var routes = require('../src/routes/routes')
const { response } = require('express')
routes(app)

//Middleware
app.use(express.json())

var server = app.listen(process.env.PORT, process.env.IP_ADDRESS, function() {
    console.log('Node Server listening on port '+process.env.PORT)
})

let adminToken
var roomName = "stanza"
let roomID
var fakeID = mongoose.Types.ObjectId()

beforeAll(async () => {
    console.log("Before all")
    await mongoose.connect(process.env.DB_CONNECT_TEST, {
          useNewUrlParser: true,
          useFindAndModify: true,
          useUnifiedTopology: true})
      .then(() => {
        console.log(`MongoDB Connected: ${process.env.DB_CONNECT_TEST}`)
        mongoose.connection.db.dropCollection('Rooms', function(err, result) {});
        console.log("rooms collection dropped")
      })
      .catch((err) => console.log(err))

    await request(app)
        .post('/api/user/register')
        .send({
        'username': 'administrator',
        'password': 'password',
        'role': 'admin'
    })

    await request(app)
    .post('/api/user/login')
    .send({
      'username': 'administrator',
      'password': 'password',
    })
    .then(response => {
      adminToken = response.header['auth-token']
    })
})


describe('Room creation', () => {
    it('should be possibile to create a room', async() => {
        await request(app)
            .post('/api/rooms/addRoom')
            .set({'auth-token': adminToken})
            .send({
                'name': roomName,
                'img': 'https://unsplash.com/photos/KxCAa7wpzu0/download?force=true&w=1920'
            })
            .expect(201)
    })

    it('should be impossibile to create a room without admin permissions', async() => {
        await request(app)
            .post('/api/rooms/addRoom')
            .send({
                'name': roomName,
                'img': 'https://unsplash.com/photos/KxCAa7wpzu0/download?force=true&w=1920'
            })
            .expect(401)
    })

    it('should be impossibile to create a room without name', async() => {
        await request(app)
            .post('/api/rooms/addRoom')
            .set({'auth-token': adminToken})
            .send({
                'img': 'https://unsplash.com/photos/KxCAa7wpzu0/download?force=true&w=1920'
            })
            .expect(400)
    })

    it('should be impossibile to create a room without image URL', async() => {
        await request(app)
            .post('/api/rooms/addRoom')
            .set({'auth-token': adminToken})
            .send({
                'name': roomName,
            })
            .expect(400)
    })
})

describe('Room read', () => {
    it('should be possible to get rooms', async () => {
        await request(app)
            .get('/api/rooms')
            .expect(200)
            .then(res => {
                res.body.forEach(room =>{
                    expect(room).toHaveProperty('name')
                    expect(room).toHaveProperty('img')
                    expect(room).toHaveProperty('link')
                    expect(room).toHaveProperty('alarm_state')
                    expect(room).toHaveProperty('info')
                    expect(room).toHaveProperty('description')
                    expect(room).toHaveProperty('devices')
                })
            })
    })

    it('should be possible to get a room by name', async () => {
        await request(app)
            .get('/api/rooms/'+roomName)
            .expect(200)
            .then(res => {
                res.body.forEach(room =>{
                    roomID = room['_id']
                    expect(room).toHaveProperty('name')
                    expect(room['name']).toBe(roomName)
                    expect(room).toHaveProperty('img')
                    expect(room).toHaveProperty('link')
                    expect(room).toHaveProperty('alarm_state')
                    expect(room).toHaveProperty('info')
                    expect(room).toHaveProperty('description')
                    expect(room).toHaveProperty('devices')
                })
            })
    })
})

describe('Room update', () => {
    it('should be possible to update name and image URL of a room', async () => {
        await request(app)
            .put('/api/rooms/updateRoom/'+roomID)
            .set({'auth-token': adminToken})
            .send({
                'name': roomName+roomName,
                'img': 'https://unsplash.com/photos/naSAHDWRNbQ/download?force=true&w=1920'
            })
            .expect(200)
            .then(res => {
                var room = res.body
                expect(room).toHaveProperty('name')
                expect(room['name']).toBe(roomName+roomName)
                expect(room).toHaveProperty('img')
                expect(room).toHaveProperty('link')
                expect(room).toHaveProperty('alarm_state')
                expect(room).toHaveProperty('info')
                expect(room).toHaveProperty('description')
                expect(room).toHaveProperty('devices')
            })
    })

    it('should be impossible to update name and image URL of a room without admin permissions', async () => {
        await request(app)
            .put('/api/rooms/updateRoom/'+roomID)
            .send({
                'name': roomName+roomName,
                'img': 'https://unsplash.com/photos/naSAHDWRNbQ/download?force=true&w=1920'
            })
            .expect(401)
    })

    it('should be impossible to update a room that does not exist', async () => {
        await request(app)
            .put('/api/rooms/updateRoom/'+fakeID)
            .set({'auth-token': adminToken})
            .send({
                'name': roomName+roomName,
                'img': 'https://unsplash.com/photos/naSAHDWRNbQ/download?force=true&w=1920'
            })
            .expect(404)
    })
})

describe('Room delete', () => {
    it('should be impossible to delete a room without admin permission', async () => {
        await request(app)
            .delete('/api/rooms/deleteRoom/'+roomID)
            .expect(401)
    })

    it('should be possible to delete a room', async () => {
        await request(app)
            .delete('/api/rooms/deleteRoom/'+roomID)
            .set({'auth-token': adminToken})
            .expect(200)
    })

    it('should be impossible to delete a room that does not exist', async () => {
        await request(app)
            .put('/api/rooms/deleteRoom/'+fakeID)
            .set({'auth-token': adminToken})
            .expect(404)
    })
})

afterAll(() => {
    // mongodb middleware tear down
    mongoose.connection.close()

    // express server tear down
    server.close()
})