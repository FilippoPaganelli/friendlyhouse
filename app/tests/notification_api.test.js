const request = require('supertest')
var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var cors = require('cors')
var path = require('path')
var env = require('dotenv')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const HttpStatus = require('http-status-codes');
env.config()
global.appRoot = path.resolve(__dirname);
app.use(bodyParser.urlencoded( {extended: false }))
app.use(bodyParser.json())
app.use(cors())
app.use('/static', express.static(__dirname + '/public'));
var routes = require('../src/routes/routes')
routes(app)

//Middleware
app.use(express.json())

app.listen(process.env.PORT, process.env.IP_ADDRESS, function() {
    console.log('Node Server listening on port '+process.env.PORT)
})

let tokenMario
let tokenLuigi

let marioToLuigi = {
  'from': 'mario',
  'to': 'luigi',
  'type': 'message',
}

let marioSystem = {
  'from': 'mario',
  'to': 'luigi',
  'type': 'system',
}

let marioWrongType = {
  'from': 'mario',
  'to': 'luigi',
  'type': 'pppp',
}

let marioToWrongUser = {
  'from': 'mario',
  'to': 'yyyyy',
  'type': 'message',
}

jest.setTimeout(30000);

beforeAll(async () => {
  console.log("Before all")
  await mongoose.connect(process.env.DB_CONNECT_TEST, {
        useNewUrlParser: true,
        useFindAndModify: true,
        useUnifiedTopology: true})
    .then( async () => {
      console.log(`MongoDB Connected: ${process.env.DB_CONNECT_TEST}`)

      mongoose.connection.db.dropCollection('Users', function(err, result) {});
      mongoose.connection.db.dropCollection('Notifications', function(err, result) {});


      await request(app)
        .post('/api/user/register')
        .send({
          'username': "mario",
          'password': "password",
          'role': 'admin'
        }).expect(HttpStatus.CREATED)
      await request(app)
        .post('/api/user/register')
        .send({
          'username': "luigi",
          'password': "password",
          'role': 'user'
        }).expect(HttpStatus.CREATED)

      await request(app)
        .post('/api/user/register')
        .send({
          'username': "gianfrancioschio",
          'password': "password",
          'role': 'user'
        }).expect(HttpStatus.CREATED)

      await request(app)
        .post('/api/user/login')
        .send({
          'username': "mario",
          'password': "password"
        })
        .expect(HttpStatus.OK)
        .then(response => {
          tokenMario = response.header['auth-token']
          console.log(tokenMario)
        })

      await request(app)
        .post('/api/user/login')
        .send({
          'username': "luigi",
          'password': "password"
        })
        .expect(HttpStatus.OK)
        .then(response => {
          tokenLuigi = response.header['auth-token']
          console.log(tokenMario)
        })
    })
    .catch((err) => console.log(err)
)});

describe("Correct behavior", () => {

  it("A user should be able to notify another user", async () => {
    await request(app)
      .post('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .send(marioToLuigi)
      .expect(HttpStatus.CREATED)
  })
  it("A user should be able to dismiss its notifications", async () => {
    let notificationId
    await request(app)
      .post('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .send(marioToLuigi)
      .then(response => {
        notificationId = response.body._id
      })

      await request(app)
        .put(`/api/notifications/users/luigi/${notificationId}`)
        .set({'auth-token': tokenLuigi})
        .expect(HttpStatus.OK)
        .then(response => {
          expect(response.body._id == notificationId)
          expect(response.body.to == "luigi")
          expect(response.body.from == "mario")
          expect(response.body.dismissed == true)
        })


  })
  it("A user should be able to retrive its notifications", async () => {
    await request(app)
      .get('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .expect(HttpStatus.OK)
  })
  it("A user should be able to retrive a specific notification", async() => {
    let notificationId
    await request(app)
      .post('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .send(marioToLuigi)
      .then(response => {
        notificationId = response.body._id
      })

    await request(app)
      .get(`/api/notifications/users/luigi/${notificationId}`)
      .set({'auth-token': tokenLuigi})
      .expect(HttpStatus.OK)
      .then(response => {
        expect(response.body._id == notificationId)
        expect(response.body.to == "luigi")
        expect(response.body.from == "mario")
      })

  })
  it("Dismissed notifications should not be retrived", async () => {
    let notificationId
    await request(app)
      .post('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .send(marioToLuigi)
      .then(response => {
        notificationId = response.body._id
      })

      await request(app)
        .put(`/api/notifications/users/luigi/${notificationId}`)
        .set({'auth-token': tokenLuigi})
        .set({'auth-token': tokenLuigi})
        .then(response => {
          expect(response.body._id == notificationId)
          expect(response.body.to == "luigi")
          expect(response.body.from == "mario")
          expect(response.body.dismissed == true)
        })

      await request(app)
        .get('/api/notifications/users/luigi')
        .set({'auth-token': tokenLuigi})
        .set({'auth-token': tokenLuigi})
        .then(response => {
          const notificationArray = response.body
          notificationArray.forEach(n => {
            expect(n.to == "luigi")
            expect(n.dismissed == false)
          })
        })
  })
  it("Dismissed notifications should be retrived if specificaly required", async () => {
    let notificationId
    await request(app)
      .post('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .send(marioToLuigi)
      .then(response => {
        notificationId = response.body._id
      })

      await request(app)
        .put(`/api/notifications/users/luigi/${notificationId}`)
        .set({'auth-token': tokenLuigi})
        .set({'auth-token': tokenLuigi})
        .then(response => {
          expect(response.body._id == notificationId)
          expect(response.body.to == "luigi")
          expect(response.body.from == "mario")
          expect(response.body.dismissed == true)
        })

      await request(app)
        .get(`/api/notifications/users/luigi/${notificationId}`)
        .set({'auth-token': tokenLuigi})
        .set({'auth-token': tokenLuigi})
        .then(response => {
          const n = response.body
          expect(n.to == "luigi")
          expect(n.dismissed == false)
        })
  })
  it("Should be possibile to create system notifications", async() => {
    await request(app)
      .post('/api/notifications/command/system/mario')
      .send()
      .expect(HttpStatus.CREATED)
  })

  it("Should be possible to broadcast a notification to every player", async() => {
    await request(app)
      .post('/api/notifications/command/system/broadcast')
      .send()
      .expect(HttpStatus.CREATED)
  })
})


describe("Scorrect behavivour", () => {
  it("Should not be possibile to submit notification without using a token", async () => {
    await request(app)
      .post('/api/notifications/users/mario')
      .send(marioToLuigi)
      .expect(HttpStatus.UNAUTHORIZED)
  })
  it("Should not be possibile to submit notification in place of another player", async() => {
    await request(app)
      .post('/api/notifications/users/luigi')
      .set({'auth-token': tokenMario})
      .send(marioToLuigi)
      .expect(HttpStatus.UNAUTHORIZED)
  })
  it("Should not be possibile to create a normal notification with system type", async() => {
    await request(app)
      .post('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .send(marioSystem)
      .expect(HttpStatus.UNAUTHORIZED)
  })
  it("Should not be possible to create a notification of an unsopported type", async() => {
    await request(app)
      .post('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .send(marioWrongType)
      .expect(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
  })
  it("Should not be possibile to submit a notification to a non existing player - player notifications", async () => {
    await request(app)
      .post('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .send(marioToWrongUser)
      .expect(HttpStatus.CONFLICT)
  })
  it("A player should not be able to close another player's notifications", async () => {
    let notificationId
    await request(app)
      .post('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .send(marioToLuigi)
      .then(response => {
        notificationId = response.body._id
      })

      await request(app)
        .put(`/api/notifications/users/mario/${notificationId}`)
        .set({'auth-token': tokenLuigi})
        .expect(HttpStatus.UNAUTHORIZED)
  })
  it("Should not be possibile to dismiss an already dismissed notification", async() => {
    let notificationId
    await request(app)
      .post('/api/notifications/users/mario')
      .set({'auth-token': tokenMario})
      .send(marioToLuigi)
      .then(response => {
        notificationId = response.body._id
      })

    await request(app)
      .put(`/api/notifications/users/luigi/${notificationId}`)
      .set({'auth-token': tokenLuigi})
      .expect(HttpStatus.OK)
      .then(response => {
        expect(response.body._id == notificationId)
        expect(response.body.to == "luigi")
        expect(response.body.from == "mario")
        expect(response.body.dismissed == true)
      })
    await request(app)
      .put(`/api/notifications/users/luigi/${notificationId}`)
      .set({'auth-token': tokenLuigi})
      .expect(HttpStatus.CONFLICT)
  })
  it("A player should not be able to get another player's specific notification", async () => {
    await request(app)
      .get(`/api/notifications/users/luigi/1234`)
      .set({'auth-token': tokenMario})
      .expect(HttpStatus.UNAUTHORIZED)
  })
  it("A player should not be able to get another player's notifications", async () => {
    await request(app)
      .get('/api/notifications/users/luigi')
      .set({'auth-token': tokenMario})
      .expect(HttpStatus.UNAUTHORIZED)
  })
  it("Should not be possibile to submit a notification to a non existing player - system notifications", async () => {
    await request(app)
      .post('/api/notifications/command/system/uuuu')
      .send()
      .expect(HttpStatus.CONFLICT)
  })
})
