# FriendlyHose #

- Filippo Paganelli - 0000926989
	- filippo.paganelli3@studio.unibo.it
- Francesco Montelli - 0000919858 
	- francesco.montelli@studio.unibo.it


## Come eseguire ##
### Requisiti ###
- Docker
- Make
### Procedimento ###
All'interno del reposotory è stato messo a disposizione un _makefile_ per consentire la messe in esecuzione del progetto in modo agevole.

Per mettere in esecuzione il progetto è necessario dare il comando `make all`

## Relaione ##

La relazione in formato PDF è stata inserita nella [sezione download](https://bitbucket.org/FilippoPaganelli/friendlyhouse/downloads/) di questo repository.

## Note ##
Il database mongo è hostato sul servizio [MongoDB Atlas](https://www.mongodb.com/cloud/atlas).

Le misure di sicurezza che potrebbero impedire l'accesso sono state disabilitate, in caso di problemi contattaci.